{-# LANGUAGE TupleSections #-}
{-# LANGUAGE BlockArguments #-}
{-|
    This module exports all the functions required by 'playGo'
    function in 'Go' game.
-}
module BoardGo(
    (|->),
    clamp',
    fuPointsKo,
    getOppositeStone,
    getPiece,
    boardPoint,
    Point(Point),
    Stone(Black, White, Ko, Empty),
    Move(Pass, Move),
    getTurnStoneFromMove,
    lastStone,
    Numbering(Numbering, enabled, numeric),
    getLabels,
    GameStatus(Alive, Dead, Over),
    Meta(Meta),
    Game(Game),
    createGame,
    getPointsKo,
    toggleKos,
    maxMovesIncrement,
    maxMovesDecrement,
    getMaxMovesFromLastMove,
    toggleLabels,
    resetNumbering,
    toggleNumbering,
    clearPending,
    pendingText,
    pendingLetter,
    usePendingLetter,
    withUndoGame,
    undoGame,
    undoGameTo,
    redoGame,
    isUndoSane,
    isRedoSane,
    playCard,
    gotoDeck,
    getDecks,
    toggleFromGS,
    libertiesOfGSAt,
    libertiesFitOfGS,
    Wire,
    toggleHighlight,
    cancelHighlight,
    reHighlightAt,
    getHighlight,
    getMultiSele,
    uniMultiSele,
    rePointHighlightAt,
    toggleKo',
    toggleZombies,
    sweepLiveZombies,
    mergeBornZombies,
    toggleDeadZombies,
    toggleLiveZombies,
    toggleZombiesSpan,
    getDeadZombies,
    getLiveZombies,
    followupAt,
    killGame,
    seekBoard,
    seekBoard',
    removePiece,
    purify,
    validating,
    playPass,
    validKo,
    validMove,
    liveGroupCapture,
    playSafe,
    playMove,
    toggleSkip,
    finishGame,
    getWinner
) where

import Data.IORef
import Data.UUID.V4 as UUIDV4
import Data.Bifunctor as Bifunctor
import Data.Function as Function
import Data.Tuple as Tuple
import Data.Bool as Bool
import Data.Maybe as Maybe
import Data.Char as Char
import Data.Map as Map hiding ((\\), disjoint, elemAt, foldl', foldr', fromList, intersection, member, null, singleton, toList, take, drop)
import Data.Set as Set hiding ((\\), foldl', foldr', notMember, null, take, drop)
import Data.List as List hiding (foldl', group, map, singleton, union)

-- Util ----------------------------------------------------------- Functions --

-- | 'to2' returs a pair with the argument as both first and second
to2 :: a -> (a, a)
to2 x = (x, x)

-- | 'least' returns the last list before the function returns an empty list
least :: ([a] -> [a]) -> [a] -> [a]
least f = fst . (until (null . snd) (second f) . to2)

-- | 'least'' returns the 'least' sets by 'Set.union' those not 'Set.disjoint'
least' :: Ord a => [Set a] -> [Set a]
least' = least part
    where part l = let n = length l - 1 in
                 [ l'i |++ l'j
                 | i <- [0..n], j <- [i+1..n],
                   let l'i = l !! i, let l'j = l !! j,
                   not $ l'i `disjoint` l'j
                 ]
                 & nub

-- | 'to1' is an alias for uncurry
to1 :: (a -> b -> c) -> ((a, b) -> c)
to1 = uncurry

-- | 'booly' has the condition as the first argument, then uncurry's
booly :: Bool -> (a, a) -> a
booly = flip (to1 bool)

-- | 'bimap1' applies 'bimap' to two identical functions
bimap1 :: Bifunctor p => (a -> b) -> p a a -> p b b
bimap1 = to1 bimap . to2

-- | 'bimap'' applies 'bimap' to just the first function
bimap' :: Bifunctor p => (a -> c) -> d -> p a b -> p c d
bimap' = curry (to1 bimap . second const)

-- Util ---------------------------------------------------------------- Bool --

infixr 2 |->
-- | '|->' is an infix operator between two 'Bool's, the first implying the second
(|->) :: Bool -> Bool -> Bool
(|->) p q = not p || q

-- Util ---------------------------------------------------------------- Sets --

-- | 'hl0' is a combined tuple of 'Set Point' constant
hl0 :: (Highlight, Set Point)
hl0 = (to2 Set.empty, Set.empty)

infixr 5 |:
-- | '|:' is an alias for 'Set.insert'
(|:) :: Ord a => a -> Set a -> Set a
(|:) = Set.insert

infixr 5 |++
-- | '|++' is an alias for 'Set.union'
(|++) :: Ord a => Set a -> Set a -> Set a
(|++) = Set.union

-- Util ---------------------------------------------------------------- Maps --

-- | 'ins0' does 'Map.insert' for key 'k' to default 'a' if missing
-- ^ or applying function if existing
ins0 :: Ord k => (a -> a) -> a -> k -> Map k a -> Map k a
ins0 f e k = to1 ($)
           . bimap (flip (Map.insert k))
                   (f . fromMaybe e . (!? k))
           . to2

-- Util ----------------------------------------------------------- List/Fold --

-- | 'foldl'' flips the order of arguments after the 'List.foldl' function
foldl' :: (b -> a -> b) -> [a] -> b -> b
foldl' = flip . List.foldl

-- | 'foldr'' flips the order of arguments after the 'List.foldr' function
foldr' :: (a -> b -> b) -> [a] -> b -> b
foldr' = flip . List.foldr

-- | 'true'fold' applies 'List.foldr' by Bool conjunction
true'fold :: Foldable t => (a -> Bool) -> t a -> Bool
true'fold fn = List.foldr (flip (&&) . fn) True

-- | 'false'fold' applies 'List.foldr' by Bool disjunction
false'fold :: Foldable t => (a -> Bool) -> t a -> Bool
false'fold fn = List.foldr (flip (||) . fn) False

-- | 'clamp' checks if the margins 'Int' and 'Int' make
-- ^ an inclusive interval for 'Int'
clamp :: Int -> Int -> Int -> Bool
clamp lt gt n = (2 ==) . length . (`List.filter` [(lt <=),(<= gt)]) $ (n &)

-- | 'clamp'' returns the 'clamp' of list's length
clamp' :: Int -> Int -> [t] -> Bool
clamp' lt gt = clamp lt gt . length

-- Spiral ----------------------------------------------------------------------

-- Adapted from source "Spiral.scala" in chapter 10 of the book:
-- "Programming in Scala" - fourth edition, by Martin Odersky, Lex Spoon, Bill Venners
-- | 'spiral'' creates a list of clockwise-ordered points as a spiral
mkSpiral :: Int -> Int -> Int -> Point -> [Point] -> [Point]
mkSpiral s d n (Point x y) ps
    | length ps == s = ps
    | otherwise =
        case d of
          0 -> ps ++ [ Point x (y-i) | i <- [1..n] ]
             & mkSpiral s 1 n (Point x (y-n))
          1 -> ps ++ [ Point (x+i) y | i <- [1..n] ]
             & mkSpiral s 2 (n+1) (Point (x+n) y)
          2 -> ps ++ [ Point x (y+i) | i <- [1..n] ]
             & mkSpiral s 3 n (Point x (y+n))
          _ -> ps ++ [ Point (x-i) y | i <- [1..n] ]
             & mkSpiral s 0 (n+1) (Point (x-n) y)

-- a spiral holds a token: at each next point, if the 'tokens' map
-- has the token, then it may pass it to the adjacent points,
-- otherwise does not
data Spiral = Spiral {
    livens :: [Point],         -- initial or reliven points
    tokens :: Map Point (),    -- points where token was passed
    passed :: Set Point        -- points which passed the token
}

-- | 'spiral' returns a 'Spiral' object
createSpiral :: Int -> Int -> Spiral
createSpiral s d = Spiral {
  livens = pt:ps
, tokens = Map.insert pt () Map.empty
, passed = Set.empty
} where pt = 1 + div (s+2*d) 2
           & to2
           & to1 Point
        ps = []
           & mkSpiral (2*(s+2*d)*(s+2*d)*2) 0 1 pt

-- Spiral -------------------------------------------------------------- Fold --

-- | 'magic' returns the four 'Int' parameters to 'spiralFold'
magic :: Point -> Int -> Int -> ((Int,Int), (Int,Int))
magic p s d = coord p
            & bimap1 (+(- (1 + div (s+2*2) 2)))
            & ((s,d),)

-- | 'spiralFold' returns the token points and the final result
-- ^ "folds" the points of a spiral with a recursive algorithm;
-- token points that did not "pass" the token reliven afresh
spiralFold :: Bool                                       -- history tokens
           -> (r -> Bool)                                -- stop condition
           -> ((Point, Set Point, r) -> (Maybe Bool, r)) -- path callback
           -> ((Int,Int), (Int,Int))                     -- size, translate coordinate
           -> r                                          -- start result
           -> Spiral                                     -- self
           -> (Set Point                                 -- token points,
              ,r)                                        -- end result
spiralFold hist stop path ((s,d),(dx,dy)) = spin
    where map' = Set.map d'tr
               . keysSet
          d'tr = to1 Point
               . bimap (+dx) (+dy)
               . coord
          trim = to1 (&&)
               . bimap1 (clamp (-d+1) (s+d))
               . bimap (+dx) (+dy)
               . coord
          spin retn this@(Spiral _ ts sp)
              | null $ livens this = ts
                                   & keysSet
                                   & (`Set.difference` sp)
                                   & toList
                                   & to2
                                   & bimap ((`bool` (map' ts, retn)) . loop)
                                           null
                                   & to1 ($)
              | pt `notMember` ts ||
                pt `member` sp = spin retn this'
              | otherwise =
                  let (stop', retn', ps) = adj'
                                         & List.foldl pass (False, retn, [])
                  in ts
                   & foldr' (`Map.insert` ()) ps
                   & to2
                   & bimap (next retn')
                           ((, retn') . map')
                   & booly stop'
              where pt = livens this & head
                    adj' = pt
                         & getAdj'
                         & List.filter trim
                         & List.filter (`notMember` ts)
                    pass (s', r', l') p
                        | s' = (s', r', l')
                        | otherwise =
                            let ts' = ts
                                    & map'
                                    & (Set.empty,)
                                    & booly hist
                         in case path (d'tr p, ts', r') of
                              (Just t, r) -> (stop r, r, l' & bool id (p:) t)
                              (_     , r) -> (True  , r, l')
                    loop ps = this { livens = ps }
                            & spin retn
                    next retn' ts' = this' { tokens = ts', passed = pt|:sp }
                                   & spin retn'
                    this' = this { livens = livens this & tail }

-- | 'spiralFold'' folds the spiral via 'spiralFold'
spiralFold' :: Bool
            -> (r -> Bool)
            -> ((Point, Set Point, r) -> (Maybe Bool, r))
            -> r
            -> Spiral
            -> ((Int,Int), (Int,Int))
            -> (Set Point, r)
spiralFold' hist stop path retn this ((s,d),(dx,dy)) =
    spiralFold hist stop path ((s,d),(dx,dy)) retn this

--------------------------------------------------------------------------------

-- | This data type represents a 'Point' in Board of Go (i.e. intersection of two lines)
data Point = Point Int Int -- ^ Point constructor takes two coordinates x and y
             deriving (Ord, Eq)

-- | 'coord' returns the coord of the 'Point' coordinates
coord :: Point -> (Int, Int)
coord (Point x y) = (x, y)

-- | 'alongBorder' checks if 'Point' is a border point
alongBorder :: Int -> Int -> Point -> Bool
alongBorder i s (Point x y) = x == i || x == s || y == i || y == s

-- | 'boardPoint' checks if 'Point' is within border
boardPoint :: Int -> Point -> Bool
boardPoint s (Point x y) = 1 <= x && x <= s && 1 <= y && y <= s

-- | 'boardBorders' returns the up-, down-, left-, right-, borders
boardBorders :: Int -> Int -> [[Point]]
boardBorders i s = [ [ Point n i, Point n s,
                       Point i n, Point s n ]
                   | n <- [i..s]
                   ]

-- | 'zombiesBorder' returns the border 'Point' next to the zombie 'Point'
zombiesBorder :: Int -> Point -> Point
zombiesBorder s (Point x y)
    | x == 0 = Point 1 y
    | x == s+1 = Point s y
    | y == 0 = Point x 1
    | y == s+1 = Point x s

-- | 'zombiesCorners' returns the corner zombie points
zombiesCorners :: Int -> Set Point
zombiesCorners s = [ Point 0 0, Point 0 (s+1), Point (s+1) 0, Point (s+1) (s+1) ]
                 & fromList

-- | This data type represents 'Stone' placed on the board
data Stone = Black -- ^ To represent 'Black' stone
           | White -- ^ To represent 'White' stone
           | Ko -- ^ 'Ko' is not shown on the board. Placed when Ko situation occurs
           | Empty -- ^ To represent that nothing is placed on the point
           deriving (Ord, Eq, Show)

-- | 'getOppositeStone' returns opposite stone of given 'Stone'
getOppositeStone :: Stone -> Stone
getOppositeStone stone | stone == Black = White
                       | stone == White = Black
                       | otherwise = Empty

-- | This data type represents a 'Move' that can be performed on the board by a player
data Move = Pass Stone -- ^ Player with this 'Stone' plays pass
          | Skip Game -- ^ Player sets a pending skip of 'Game'
          | Move Int Point Stone -- ^ Player with this 'Stone' plays 'Int'th on a 'Point'

-- | 'getPiece' returns the 'Stone' from a given 'Move'
getPiece :: Move -> Stone
getPiece (Pass st) = st
getPiece (Skip Game{lastMove=lm}) = getPiece lm
getPiece (Move _ _ st) = st

-- | 'getMoveCount' returns the number of a given 'Move' (1st, 2nd, ...)
getMoveCount :: Move -> Int
getMoveCount (Pass _) = 0
getMoveCount (Move n _ _) = n

-- | 'getTurnStoneFromMove' returns the stone whose turn is
-- ^ from given 'Move' with maximum 'Int' moves
getTurnStoneFromMove :: Move -> Int -> Stone
getTurnStoneFromMove lm@(Skip _) _ = getOppositeStone (getPiece lm)
getTurnStoneFromMove lm m = getPiece lm
                          & if mod (getMoveCount lm) m == 0
                            then getOppositeStone else id

-- | 'lastStone' returns the 'Point' of (and the) last 'Stone'
lastStone :: Game -> Maybe (Point, Stone)
lastStone Game{meta=md,lastMove=lm} =
    case lm of
      Skip game            -> lastStone game
      Move _ (Point 0 0) _ -> Nothing
      Move _ pt st         -> Just (pt, st)
      _                    -> lastStone (undo md & purify)

-- Numbering/Labels ------------------------------------------------------------

-- point/stone labels are letters or numbers
data Labelling = Letter Char
               | Number Int
                 deriving (Eq)

-- a numbering can be dual - either letters or numbers
data Numbering = Numbering {
  enabled :: Bool
, numeric :: Bool
, labelling :: Map Point Labelling
, totalCount :: Int
} deriving (Eq)

-- | 'createNumbering' creates a 'Numbering' object
createNumbering :: Numbering
createNumbering = Numbering {
  enabled = True
, numeric = True
, labelling = Map.empty
, totalCount = 0
}

-- | 'incNumbering' increments total count adding consecutive label at 'Point'
incNumbering :: Numbering -> Point -> Numbering
incNumbering nr@(Numbering e n ll tc) point =
    if n && tc == 9999 || not n && tc == ord 'z' - ord 'a' + 1
    then nr else nr'
    where l = if n then Number (1 + tc) else Letter (chr (ord 'a' + tc))
          ll' = addPiece l point ll
          nr' = Numbering{enabled=e,numeric=n,labelling=ll',totalCount=tc+1}

-- | 'clrNumbering' removes the labels in current numbering,
-- ^ while not removing the labels in the dual numbering
clrNumbering :: Numbering -> Numbering
clrNumbering nr@Numbering{numeric=n,labelling=ll} =
    nr{labelling=ll',totalCount=0}
    where up l = case l of
                   Letter _ | not n -> Nothing
                   Number _ | n     -> Nothing
                   _                -> Just l
          ll' = keys ll & List.foldl (flip (Map.update up)) ll

-- | 'tglNumbering' toggles between numeric and alphabetic numbering
tglNumbering :: Numbering -> Numbering
tglNumbering nr@Numbering{numeric=n,labelling=ll} =
    nr{numeric=n',labelling=ll,totalCount=tc'}
    where n' = not n
          tc' = Map.foldl m 0 ll
          m r l = case l of
                    Letter c | n  -> max r (ord c - ord 'a' + 1)
                    Number i | n' -> max r i
                    _             -> r

-- Numbering --------------------------------------------------------- Labels --

-- | 'setLetterLabel' updates labelling with a letter at 'Point'
setLetterLabel :: Numbering -> Char -> Point -> Numbering
setLetterLabel nr@Numbering{labelling=ll} c p = nr{labelling=ll'}
    where pipe = const . Just . Letter . toLower
          ll' = Map.update (pipe c) p ll

-- | 'getLabels' returns a list of pairs - be it numbers or 'Char.ord's
getLabels :: Bool -> Numbering -> [(Point, Int)]
getLabels _ Numbering{enabled=False} = []
getLabels n Numbering{labelling=ll}
    | n = Map.mapWithKey nr ll
        & Map.elems
        & purifyList
    | otherwise = Map.mapWithKey lr ll
                & Map.elems
                & purifyList
    where nr p l = case l of
                     Letter _ -> Nothing
                     Number i -> Just (p, i)
          lr p l = case l of
                     Number _ -> Nothing
                     Letter c -> Just (p, ord c)

--------------------------------------------------------------------------------

type Highlight = (Set Point, Set Point) -- group, liberties
type MultiSele = [Highlight] -- at most 4

type GlueWire = [(Wire, (Int, Int))]

-- | This data type represents a 'Tree' of deck to cards 'Game's
-- ^ (through the 'uuid' metadata fields)
type Tree = Map String (Map Point Game)

-- | This data type represents 'Meta' data
data Meta = Meta {
  uuid :: Maybe String
, spiral :: Spiral
, numbering :: Numbering
, pending :: Maybe String -- ^ 1-char label letter | 2-char op-code | pressed-key hint
, highlight :: (Bool,Stone,MultiSele) -- ^ either 1 group at stone or <=4 groups at point
, groupsSelected :: (GlueWire, MultiSele)
, undo :: Maybe Game
, redo :: Maybe Game
, pointsKo' :: [Point]
, sanityKo :: Bool
, deadZombies :: Maybe (Map Point Stone)
, bornZombies :: Map Point Stone
, liveZombies :: Maybe (Map Point Stone)
, followup :: Numbering
, tree :: IORef Tree
, grid :: [Point]
, showHelp :: Bool
}

-- | 'createMeta' returns a 'Meta' object
createMeta' :: Int -> Meta
createMeta' size = Meta {
  uuid = Nothing
, spiral = createSpiral size 2
, numbering = createNumbering
, pending = Nothing
, highlight = (True, Empty, [])
, groupsSelected = ([], [])
, undo = Nothing
, redo = Nothing
, pointsKo' = []
, sanityKo = True
, deadZombies = Nothing
, bornZombies = Map.empty
, liveZombies = Nothing
, followup = createNumbering
, grid = [ Point x y | x <- [1..size], y <- [1..size] ]
, showHelp = True
}

-- | 'createMeta' returns a 'Meta' object via 'createMeta''
createMeta :: Int -> IO Meta
createMeta size = do
  tr <- newIORef Map.empty
  return $ (createMeta' size){tree=tr}

-- | This data type represents 'Game'
data Game = Game {
  meta :: Meta
, maxMoves :: Int
, pointsKo :: (Bool, [Point])
, board :: Map Point Stone -- ^ Map from point to stone
, lastMove :: Move -- ^ Last move that was played in the game
, boardSize :: Int -- ^ Board size 9 or 13 or 19
, scoreBlack :: Int -- ^ Score of Black stone
, scoreWhite :: Int -- ^ Score of White stone
, status :: GameStatus -- ^ Status of the game whether game is running or over
}

-- | 'GameStatus' represents state of the game if game is alive or dead or over
data GameStatus = Alive -- ^ game is running
                | Dead -- ^ game is over and hopeless string is counted
                | Over -- ^ Game over and results are shown on the screen
                deriving (Eq)

-- | 'createGame'' takes a size and returns a 'Game' object
createGame' :: Int -> Game
createGame' size = Game {
  maxMoves = 1
, pointsKo = (False, [])
, board = Map.empty
, lastMove = Move 0 (Point 0 0) White
, boardSize = size
, scoreBlack = 0
, scoreWhite = 0
, status = Alive
}

-- | 'createGame' takes a size and returns a 'Game' object via 'createGame''
createGame :: Int -> IO Game
createGame size = do
  md <- createMeta size
  addDeck (createGame' size){meta=md}

-- Tree ------------------------------------------------------------------------

-- | 'fuPointsKo' returns the followup-"Ko" points for a 'Game'
fuPointsKo :: Game -> IO [Point]
fuPointsKo Game{meta=md@Meta{tree=rt}} =
    case uuid md of
      Just is -> do
                 tr <- readIORef rt
                 return $ keys (tr ! is)
      _       -> return []

-- | 'gcTree' deletes dangling indices in the 'Tree' (through 'redo' metadata field)
-- ^ including the followup cards
gcTree :: Game -> IO ()
gcTree Game{lastMove=Skip game} = gcTree game
gcTree Game{meta=Meta{redo=Just game@Game{meta=md@Meta{tree=tr}}}} = do
    gcTree game
    case uuid md of
      Just is -> do
                 modifyIORef tr (removeGame is)
      _       -> pure ()
gcTree _  = pure ()

-- | 'addGame' adds a game to the 'Tree' of decks and/or cards
addGame :: String -> Tree -> Map Point Game -> Tree
addGame = to1 (.) . bimap (flip . Map.insert) Map.delete . to2

-- | 'removeGame' removes a 'String'-deck together with its cards from the 'Tree'
removeGame :: String -> Tree -> Tree
removeGame = to1 (.) . bimap Map.delete removeCards . to2

-- | 'removeCards' removes the cards of 'String'-deck from the 'Tree'
removeCards :: String -> Tree -> Tree
removeCards is = to1 ($)
               . bimap (List.foldr removeGame)
                       (purifyList
                       . fmap (uuid . meta)
                       . Map.elems
                       . (! is))
               . to2

-- Multiple moves --------------------------------------------------------------

-- | 'getPointsKo' returns the number of Ko points (at most one if disabled)
getPointsKo :: Game -> [Point]
getPointsKo Game{pointsKo=(eko,kos)} =
    if eko then kos
    else maybe [] (replicate 1 . fst) (uncons kos)

-- | 'toggleKos' enables or disables multiple (possibly more than one) Ko
toggleKos :: Game -> Game
toggleKos game@Game{board=m,pointsKo=(False,kos)} =
    game{board=foldr' (addPiece Ko) kos m,pointsKo=(True,kos)}
toggleKos game@Game{board=m,pointsKo=(_,p:kos)} =
    game{board=foldl' removePiece kos m,pointsKo=(False,p:kos)}
toggleKos game = game{pointsKo=(False,[])}

-- | This function returns the maximum number of moves after and from last 'Move'
getMaxMovesFromLastMove :: Game -> Int
getMaxMovesFromLastMove (Game _ m _ _ (Skip _) _ _ _ _) = m
getMaxMovesFromLastMove (Game _ m _ _ lm _ _ _ _) = m - mod (getMoveCount lm) m

-- | 'maxMovesIncrement' increments the maximum number of moves (not more than liberties)
maxMovesIncrement :: Game -> Game
maxMovesIncrement game@Game{maxMoves=m,lastMove=lm,boardSize=size,status=Alive}
    | m < s =
      case lm of
        Move n pt st | n == m -> game'{lastMove=Move m' pt st}
        _                     -> game'
    where m' = m+1
          game'=game{maxMoves=m'}
          s = size*size
maxMovesIncrement game = game

-- | 'maxMovesDecrement' decrements the maximum number of moves (not less than one)
maxMovesDecrement :: Game -> Game
maxMovesDecrement game@Game{maxMoves=m,lastMove=lm,status=Alive}
    | m > 1 =
      case lm of
        Move n pt st | n == m  -> game'{lastMove=Move m' pt st}
        Move n _ _   | n == m' -> nextTurn False game'
        _                      -> game'
    where m' = m-1
          game'=game{maxMoves=m'}
maxMovesDecrement game = game

-- Labels/Numbering ------------------------------------------------------------

-- | 'toggleLabels' toggles visibility of labels (either stones or followups)
toggleLabels :: Bool -> Game -> Game
toggleLabels f game@Game{meta=md} = game{meta=md'}
    where Meta{numbering=nr,followup=fu} = md
          md' | f = md{followup=fu{enabled=not (enabled fu)}}
              | otherwise = md{numbering=nr{enabled=not (enabled nr)}}

-- | 'resetNumbering' clears all labels in the numbering (either stones or followups)
resetNumbering :: Bool -> Game -> IO Game
resetNumbering f game@Game{meta=md@Meta{tree=tr}} =
    case uuid md of
      Just is -> if f
                 then do
                   modifyIORef tr (removeGame is)
                   return game{meta=md'{uuid=Nothing}}
                 else
                   return game'
      _       -> return game'
    where Meta{numbering=nr,followup=Numbering{enabled=e,numeric=n}} = md
          md' | f = md{followup=createNumbering{enabled=e,numeric=n}}
              | otherwise = md{numbering=clrNumbering nr}
          game' = game{meta=md'}

-- | 'toggleNumbering' swaps between numeric vs alphabetic numbering (stones or followups)
toggleNumbering :: Bool -> Game -> IO Game
toggleNumbering f game@Game{meta=md} = do
    kos <- fuPointsKo game
    return case () of
             _ | not f    -> game{meta=md'}
             _ | null kos -> game{meta=md''}
             _            -> game
    where Meta{numbering=nr,followup=Numbering{enabled=e,numeric=n}} = md
          md' = md{numbering=tglNumbering nr}
          md'' = md{followup=createNumbering{enabled=e,numeric=not n}}

-- Pending ---------------------------------------------------------------------

-- | 'clearPending' clears the pending flag
-- a pending text can be:
-- a letter label - used for an existing or next stone, and is prioritized; or
-- a key hint - a suggestive word that appears in the status text while key is down
--              and disappears after key is released; or
-- an op-code - dynamic highlighting on motion or static multi-selection of groups:
--              can be suspended during a pending hint and restored afterwards
clearPending :: Bool -> Game -> Game
clearPending pop game@Game{meta=md@Meta{pending=Just t}}
    | pop && n > 0 = game{meta=md'}
    | otherwise = game{meta=md{pending=Nothing}}
    where n = fromMaybe 0 (elemIndex ' ' t)
          md' = md{pending=Just (drop (n+1) t)}
clearPending _ game = game

-- | 'pendingText' sets the pending op-code or pressed-key hint to 'String'
pendingText :: String -> Game -> Game
pendingText "" game = clearPending True game
pendingText text game@Game{meta=md@Meta{pending=pl}} =
    game{meta=md{pending=Just (text ++ maybe "" (" " ++) pl)}}

-- Pending ------------------------------------------------------------ Label --

-- | 'pendingLetter' sets the pending text to just a letter
-- (max moves must be 1)
pendingLetter :: Game -> Char -> Game
pendingLetter game@(Game md@Meta{numbering=nr@Numbering{numeric=True}} 1 _ _ _ _ _ _ Alive) c
    | 'a' <= c' && c' <= 'z' = game{meta=md{numbering=nr{enabled=True},pending=Just [c']}}
    where c' = toLower c
pendingLetter game _ = game

-- | 'usePendingLetter' labels the stone at 'Point' using the pending letter
usePendingLetter :: Point -> Game -> Game
usePendingLetter p game@Game{meta=md@Meta{pending=Just [c]}} = game{meta=md'}
    where Meta{numbering=nr} = md
          md' = md{pending=Nothing,numbering=setLetterLabel nr c p}
usePendingLetter _ game = game

-- Undo/Redo -------------------------------------------------------------------

-- | 'withUndoGame' sets the 'undo' field to 'Game' in 'Game' metadata
withUndoGame :: Game -> Game -> Game
withUndoGame Game{lastMove=Skip game'} game = withUndoGame game' game
withUndoGame game' game@Game{meta=md} = game{meta=md{undo=Just game'}}

-- | 'withLookGame' copies the look (or visual appearance) from 'Game' to 'Game'
withLookGame :: Game -> Game -> Game
withLookGame Game{meta=look,pointsKo=(eko,_),maxMoves=m} game = game'
    where Meta _ _ Numbering{enabled=e} _ (ehl,_,_) _ _ _ _ sko zs _ sz Numbering{enabled=f} _ _ h = look
          Game{meta=md@Meta{numbering=nr,followup=fu},pointsKo=(_,kos),lastMove=lm} = game
          game' = game{meta=md{numbering=nr{enabled=e}
                              ,highlight=(ehl,Empty,[])
                              ,groupsSelected=([],[])
                              ,sanityKo=sko
                              ,liveZombies=sz
                              ,followup=fu{enabled=f}
                              ,showHelp=h}
                      ,pointsKo=(eko,kos)
                      ,maxMoves=m'}
                & bool mirrorZombies id (isNothing zs && isNothing sz)
          m' = case lm of
                 Move n _ _ | n > m -> maxMoves game
                 _                  -> m

-- | 'withLookGame'' (prime) possibly adds Ko' points after 'withLookGame'
withLookGame' :: Game -> Game -> Game
withLookGame' game@Game{meta=Meta{sanityKo=sko}} = pipe
    where pipe = bool id playKo' sko . removeKo' . withLookGame game

-- | 'withRedoGame'' (prime, for Ko' points) sets the redo field after 'withLookGame''
withRedoGame' :: Game -> Game -> Game
withRedoGame' game' game@Game{meta=md} =
    withLookGame' game' game{meta=md{redo=Just game'}}

-- | 'withRedoGame' sets the redo field after 'withLookGame'
withRedoGame :: Game -> Game -> Game
withRedoGame game' game@Game{meta=md} =
    withLookGame game' game{meta=md{redo=Just game'}}

-- | 'isSaneGame' checks if there are no Ko' points in 'Game'
isSaneGame :: Game -> Bool
isSaneGame game@Game{meta=md@Meta{pointsKo'=kos',sanityKo=sko}} =
    null $ bool (pipe' game') kos' sko
    where game' = game{meta=md{sanityKo=True}}
          pipe' = pointsKo' . meta . playKo'

------------------------------------------------------------------------ Undo --

-- | 'undoGame' does undo for the last [multi-moves] stone
undoGame :: Game -> IO Game
undoGame game@Game{lastMove=Skip _} = undoGame (toggleSkip game)
undoGame game@Game{meta=Meta{undo=Just game'@Game{status=Dead}}} =
    return $ withRedoGame game game'
undoGame game@Game{meta=Meta{undo=Just game'@Game{lastMove=Pass _}}} =
    return $ withRedoGame' game game'
undoGame game@Game{meta=Meta{undo=Just game'@Game{lastMove=Move{}}}} = do
    kos <- fuPointsKo game'
    case () of
      _ | tc == length kos -> return $ withRedoGame' game game'
      _                    -> withoutRedoGame game
                            & withLookGame' game
                            & undoGameTo'Deck game' game
    where Game{meta=Meta{followup=fu}} = game'
          Numbering{totalCount=tc} = fu
undoGame game = return game

-- | 'undoGame'' occurs from 'isUndoSane' and does just undo (through 'undo' metadata field)
undoGame' :: Game -> Game
undoGame' Game{lastMove=Skip game} = undoGame' game
undoGame' Game{meta=Meta{undo=Just game'}} = game'
undoGame' game = game

-- | 'isUndoSane' checks for sanity of an undo (if sanity check was intermittent)
isUndoSane :: Game -> Bool
isUndoSane Game{meta=Meta{undo=Nothing}} = True
isUndoSane game@Game{meta=Meta{sanityKo=sko}} = sko |-> isSaneGame game'
    where game' = undoGame' game

------------------------------------------------------------------------ Redo --

-- | 'redoGame' does redo to what 'undoGame' did
redoGame :: Game -> Game
redoGame game@Game{lastMove=Skip _} =
    redoGame (toggleSkip game)
redoGame game'@Game{meta=Meta{redo=Just game@Game{status=Dead}}} =
    withLookGame game' game
redoGame game'@Game{meta=Meta{redo=Just game}} =
    withLookGame' game' game
redoGame game' = game'

-- | 'redoGame'' occurs from 'isRedoSane' and does just redo (through 'redo' metadata field)
redoGame' :: Game -> Game
redoGame' Game{lastMove=Skip game} = redoGame' game
redoGame' Game{meta=Meta{redo=Just game'}} = game'
redoGame' game = game

-- | 'isRedoSane' checks for sanity of a redo (if sanity check was intermittent)
isRedoSane :: Game -> Bool
isRedoSane Game{meta=Meta{redo=Nothing}} = True
isRedoSane game@Game{meta=Meta{sanityKo=sko}} = sko |-> isSaneGame game'
    where game' = redoGame' game

-- Without -------------------------------------------------------- Undo/Redo --

-- | 'withoutUndoGame' returns a 'Game' without undo, and the one just following
withoutUndoGame :: Game -> (Game, Maybe Game)
withoutUndoGame game'@Game{meta=Meta{undo=Just game}}
    = withRedoGame' game' game
    & to2
    & bimap (, Just game')
            (const . withoutUndoGame)
    & flip (to1 maybe) (undo . meta $ game)
withoutUndoGame game = (game, Nothing)

-- | 'withoutRedoGame' returns a 'Game' without redo
withoutRedoGame :: Game -> Game
withoutRedoGame Game{meta=Meta{redo=Just game}} = withoutRedoGame game
withoutRedoGame game = game

--------------------------------------------------------------------- Undo To --

-- | 'undoGameTo' does undo to the game where move at 'Point' was the last stone
undoGameTo :: Point -> Game -> IO Game
undoGameTo p game@Game{lastMove=Move _ p' _}
    | p == p' = return game -- now
undoGameTo p game@Game{lastMove=Skip _} = do
    to <- undoGameTo' game' game' p game'
    return $ fromMaybe game to
    where game' = toggleSkip game
undoGameTo p game = do
    to <- undoGameTo' game game p game
    return $ fromMaybe game to

-- | 'undoGameTo'' does the undo (through 'undo' metadata field)
undoGameTo' :: Game -> Game -> Point -> Game -> IO (Maybe Game)
undoGameTo' Game{meta=Meta{undo=Nothing}} _ _ _ = pure Nothing
undoGameTo' game@Game{meta=md,lastMove=Move _ p' _} card p from
    | p == p'   = do
                  deck <- undoGameTo'Deck game card from
                  return $ Just deck
    | otherwise = undoGameTo' (purify . undo $ md) game p from
undoGameTo' game@Game{meta=Meta{undo=Just game'}} _ p from =
    undoGameTo' game' game p from

-- | 'undoGameTo'Deck' occurs after 'undoGame' or matching-'undoGameTo''
-- ^ and does plain undo, unless 'Game' is a deck prior to 'Game' for
-- ^ yet not (via 'addCard') added 'Game'-card
undoGameTo'Deck :: Game -> Game -> Game -> IO Game
undoGameTo'Deck game@Game{meta=md} game' card = do
    kos <- fuPointsKo deck
    maybe (return deck) (addCard card deck) (deal game' kos)
    where Meta{followup=Numbering{labelling=ll}} = md
          deal Game{lastMove=Move _ pt _} kos
              | pt `elem` keys ll &&
                pt `notElem` kos = Just pt
          deal _ _ = Nothing
          deck = withRedoGame' card game

-- Deck of Cards ---------------------------------------------------------------

-- | 'addDeck' occurs from 'somePlay' via 'best' point
addDeck :: Game -> IO Game
addDeck game@Game{meta=md@Meta{tree=rt}} = do
    uuid'v4 <- UUIDV4.nextRandom
    let is = show uuid'v4
    tr <- readIORef rt
    let tr' = addGame is tr Map.empty
    writeIORef rt tr'
    return $ game{meta=md{uuid=Just is}}

-- | 'addCard' adds the 'Game'-card to 'Game'-deck at already followup-"Ko" 'Point'
-- ^ and returns the deck
addCard :: Game -> Game -> Point -> IO Game
addCard card deck@Game{meta=md@Meta{uuid=Just is,tree=rt}} p = do
    tr <- readIORef rt
    let tr' = addGame is tr (addPiece card p (tr ! is))
    writeIORef rt tr'
    return $ deck{meta=md{redo=Nothing}}

-- | 'playCard' returns the 'Game'-card at 'Point'
playCard :: Point -> Game -> IO Game
playCard point deck@Game{meta=Meta{uuid=Just is,tree=rt}} = do
    tr <- readIORef rt
    let card = (tr ! is) ! point
    return $ withLookGame' deck card

-- | 'gotoDeck' returns the partially applied function 'undoGameTo' at 'Point', if
-- ^ a 'Game'-deck at 'Point' has been found via 'getDecks', otherwise the game
-- ^ without a stone via 'withoutUndoGame'
gotoDeck :: Point -> Game -> (Game -> IO Game)
gotoDeck point from = do
    let deck = List.find ((== point) . fst . fst) decks
    maybe none (const (undoGameTo point)) deck
    where decks = getDecks True from
          none game@Game{lastMove=lm} =
              case withoutUndoGame from' of
                (deck', Just game') -> undoGameTo'Deck deck' game' from'
                (deck', _         ) -> pure deck'
              where from' = pipe game
                    pipe = case lm of Skip _ -> toggleSkip
                                      _      -> id

-- | 'getDecks' returns all stone-deck points present on 'Game'
-- ^ the label now, if any, is paired with the 'Game'-deck
getDecks :: Bool -> Game -> [((Point, Stone), (Maybe Int, Game))]
getDecks each Game{lastMove=Skip game} = getDecks each game
getDecks _ Game{meta=Meta{undo=Nothing}} = []
getDecks each from@Game{meta=Meta{grid=points}}
    = to1 (++)
    . second (flip (bool []) each)
    . bimap (flip (get' from True Set.delete ord' Set.empty) [])
            (flip (get' from False Set.insert (const Nothing) Set.empty) [])
    . to2
    $ bs
    where bs = to1 (bool id) . bimap (|:) (/= Empty)
             & curry
             & Map.foldrWithKey
             & to1
             $ (Set.empty, board from)
          non'bs = fromList points `Set.difference` bs
          get' Game{meta=Meta{undo=Nothing}} _ _ _ _ _ qs = qs
          get' _ _ _ _ _ ps qs | null ps = qs
          get' game@Game{meta=md@Meta{numbering=nr},lastMove=lm} mt pp lb vs ps qs =
              case lm of
                Move _ pt st
                  | pt `member` ps == mt &&
                    not (mt && pt `member` fs) -> lb (nr, pt)
                                                & (, game)
                                                & ((pt, st), )
                                                & (:)
                                                & (id, )
                                                & booly (isJust is)
                                                & ($ qs)
                                                & get' game' mt pp lb fs (pp pt ps)
                _                              -> get' game' mt pp lb fs ps qs
              where fs = getSafePoints game
                       & fromList
                       & (`Set.difference` non'bs)
                       & (vs|++)
                       & (vs, )
                       & booly mt
                    Meta{uuid=is,undo=Just game'} = md
          ord' (nr, pt) = case labelling nr !? pt of
                            Just (Letter l) -> Just (ord l)
                            Just (Number l) -> Just (-l)
                            _               -> Nothing

-- Find Game ----------------------------------------------------------- Undo --

-- | 'sameGame' checks if two boards of 'Game' and 'Game' are equal piecewise
-- ^ (empty at Ko and Ko' points, disregarding the followup-"Ko" points)
sameGame :: Game -> Game -> Bool
sameGame game game1 = m' == m1'
    && true'fold ((== Empty) . seekBoard' m1) points
    && true'fold ((== Empty) . seekBoard' m) points1
    where Game{meta=Meta{pointsKo'=kos'},board=m} = game
          Game{meta=Meta{pointsKo'=kos1'},board=m1} = game1
          points = getPointsKo game ++ kos'
          m' = m
             & keys
             & (\\ points)
             & fromList
             & restrictKeys m
          points1 = getPointsKo game1 ++ kos1'
          m1' = m1
              & keys
              & (\\ points1)
              & fromList
              & restrictKeys m1

-- | 'findGame' finds a game (through 'undo' metadata field) with identical board
-- ^ any two boards are compared via 'sameGame' - the more games the slower
findGame :: Game -> Game -> Bool
findGame Game{lastMove=Skip game} = findGame' game
findGame game = findGame' game

-- | 'findGame'' occurs from 'findGame'
findGame' :: Game -> Game -> Bool
findGame' game@Game{meta=Meta{undo=Just game'}} game1 =
    sameGame game game1 || findGame' game' game1
findGame' _ _ = False

-- Ko' -------------------------------------------------------------------------

-- | 'validating' validates move(s) at 'Point' and returns a function
-- ^ that applied to 'Game' would play the valid move(s)
validating :: Game -> Point -> Maybe (Game -> Game)
validating game@Game{meta=Meta{pointsKo'=kos'},board=m} pt
    | pt `elem` kos ++ kos' ++ keys m = Nothing
    | validMove game pt = play'move
    | clamp' 1 moves lbs'lg && safe lbs'lg = play'safe lbs'lg
    | clamp' 1 moves lbs'gs && safe lbs'gs = play'safe lbs'gs
    | otherwise = Nothing
    where kos = getPointsKo game
          moves = getMaxMovesFromLastMove game
          lbs'lg = liveGroupCapture game pt moves
          lbs'gs = libertiesOfGSAt (Just pt) game & snd
          safe lbs = null $ lbs `intersect` kos ++ kos'
          play'move = Just (playing $ playMove' pt)
          play'safe ps = Just (playing $ playSafe' ps)

-- | 'toggleKo'' toggles between sanity Ko' (prime) points vs speed (no check)
toggleKo' :: Game -> Game
toggleKo' game@Game{meta=md@Meta{sanityKo=sko}} =
    game' & if sko then removeKo' else playKo'
    where game' = game{meta=md{sanityKo=not sko}}

-- | 'playKo'' (possibly) adds sanity Ko [prime] points
-- board Ko' points must be priory removed via 'removeKo'' unless disabled
-- occurs from 'toggleKo'', 'playMove'', 'voidPlay', or 'withLookGame''
playKo' :: Game -> Game
playKo' game@Game{meta=Meta{sanityKo=False}} = game
playKo' game@Game{meta=md@Meta{grid=points},board=m}
    | null ps' = game
    | otherwise = addKo' game ps'
    where ps' = foldr' kos' points' [] & purifyList
          points' = points \\ (getPointsKo game ++ keys m)
          kos' pt = (:)
                  . bool Nothing (Just pt)
                  . findGame game'
                  . ($ game')
                  & flip (maybe id) (validating game' pt)
          game' = game{meta=md{sanityKo=False,pointsKo'=[]}}

-- | 'removeKo'' removes 'Ko' [prime] from the 'Game'
removeKo' :: Game -> Game
removeKo' (Game md@Meta{pointsKo'=kos'} mm kos m lm s b w status) =
    Game md' mm kos newBoard lm s b w status
    where newBoard = List.foldl removePiece m kos'
          md' = md{pointsKo'=[]}

-- Groups Selected -------------------------------------------------------------

-- | 'toggleFromGS' adds/remove group of stone at 'Point' to/from selected
toggleFromGS :: Point -> Game -> Game
toggleFromGS point game@Game{meta=md@Meta{groupsSelected=(_,gs)}}
    | null hl = findGroup game st point
              & (:gs)
              & game'
    | otherwise = game' (gs \\ hl)
    where st = seekBoard game point
          hl = gs
             & List.filter (elem point . fst)
          game' gs' = let md' = md{groupsSelected=([],gs')}
                   in let gw = glueWire . getGSWire $ game{meta=md'}
                   in game{meta=md{groupsSelected=(gw,gs')}}

-- | 'libertiesOfGSAt' returns the liberties from selected groups
-- ^ that share the same liberty 'Point', with it as head
libertiesOfGSAt :: Maybe Point -> Game -> (Set Point, [Point])
libertiesOfGSAt _ Game{meta=Meta{groupsSelected=(_,[])}} = (Set.empty, [])
libertiesOfGSAt point Game{meta=Meta{groupsSelected=(_,gs)}}
    | null ps = (Set.empty, [])
    | otherwise = gs
                & fmap snd
                & Set.unions
                & Set.delete pt
                & toList
                & (pt:)
                & (ps,)
    where pt = fromMaybe (elemAt 0 ps) point
          ps = gs
             & fmap snd
             & to2
             & bimap tail head
             & to1 (foldr' intersection)

-- | 'libertiesFitOfGS' returns the shared liberties of selected groups
-- ^ that are fit for multi-moves via 'libertiesOfGSAt'
libertiesFitOfGS :: Game -> IO [Point]
libertiesFitOfGS Game{meta=Meta{groupsSelected=(_,[])}} = return []
libertiesFitOfGS game@Game{meta=Meta{pointsKo'=kos'}} = do
    kos'' <- fuPointsKo game
    let kos = getPointsKo game ++ kos' ++ kos''
    let safe = null . intersect kos
    let (fs, ps) = libertiesOfGSAt Nothing game
    return $ ps
           & to2
           & bimap (clamp' 1 moves) safe
           & to1 (&&)
           & bool [] (toList fs)
    where moves = getMaxMovesFromLastMove game

-- Wire ------------------------------------------------------------------------

-- | The same wire can come from two 'Point's, but with flipped deltas;
-- ^ from adding the delta to the wire point, it results its stone point
type Wire = (Point, (Int,Int))

-- | 'wires2lbs' returns the (liberty) points of the wires without 'nub'ing
wires2lbs :: [Wire] -> [Point]
wires2lbs = fmap fst

-- | 'getWire' returns the wires that are "open" and the liberties
-- ^ for which the wires at these points are "closed";
-- ^ it creates the wire around a group of stones,
-- ^ excluding the "closed" wires via 'getWire''; a square wire
-- ^ is created around each stone in the group, then
-- ^ the wires common to any two adjacent stones are removed
getWire :: Game -> Highlight -> ([Wire], Set Point)
getWire _ (grp,_) | null grp = ([], Set.empty)
getWire game@Game{boardSize=s} (grp, lbs)
    = ws
    & getWire' game (grp', lbs') (null zs'grp)
    & second (fromList . wires2lbs)
    where st = seekBoard game (elemAt 0 grp)
          square = flip zip [(0,1), (0,-1), (1,0), (-1,0)]
                 . getAdj'
          ws = grp'
             & concatMap square
             & to2
             & second (List.filter ((`elem` grp') . fst))
             & to1 (\\)
          (grp', lbs') = (grp |++ fr'grp |++ zs'grp, lbs |++ fr'lbs |++ zs'lbs)
          ((fr'grp, fr'lbs), zs'grp) = hl0
                                     & maybe
                                     & to1
                                     $ (mostZombies game st (grp, lbs), zs)
          zs'lbs = near zs'grp |++ to1 (|++) (ends zs)
          near = Set.filter ((== Empty) . seekBoard' (board game))
               . Set.map (zombiesBorder s)
               . (`Set.difference` zombiesCorners s)
          ends = maybe (Set.empty, Set.empty)
                       (bimap (end' (fst, take 2))
                              (end' (snd, drop 2))
                       . to2)
          end' (f, t) m = (`elem` [0, s+1]) . f . coord
                        & (`Set.filter` zs'grp)
                        & toList
                        & concatMap (t . getAdj')
                        & fromList
                        & Set.filter (not
                                     . to1 (||)
                                     . bimap (alongBorder (-1+0) ((s+1)+1))
                                             ((/= Empty) . seekBoard' m)
                                     . to2)
          zs = getZombies game

-- | 'getWire'' returns the "open" and the "closed" wires
-- ^ it excludes "closed" wires inside holes (a hole is
-- ^ common to nested groups; there can be "disjoint" holes)
getWire' :: Game -> Highlight -> Bool -> [Wire] -> ([Wire], [Wire])
getWire' game@Game{board=m} (grp, lbs) no'zs ws =
    (ws \\ ws', ws')
    where ws' = (`elem` lbs') . fst
              & (`List.filter` ws)
          lbs' = bool 2 1 no'zs
               & getHoles game m' (grp, lbs)
               & fst
          m' = bool (getZombies game) Nothing no'zs
             & zombies'
             & Map.union m

-- Wire ----------------------------------------------------- Multi Selection --

type RenderWire = ((Set Wire, Set Point)
                  ,(Map Wire (Set Point), Map Point [Set Point]))

-- | 'getGSWire' returns the wire for the multi-selection in groups selected
-- ^ and the ("closed") liberty points from wires that have been removed
-- (rendering wire glue requires also a mapping of wire to its group)
-- the wires "inside" a [unique disjoint] hole(s) (since all stones belong only
-- to the groups selected) are removed - unless there are nested groups,
-- which (after linearization) are "restored" the wires
-- (rendering wire glue would also require a second mapping of liberty to its group)
getGSWire :: Game -> RenderWire
getGSWire game@Game{meta=Meta{groupsSelected=(_,gs@(_:_))}} =
    let ws'o = fst ws'oc
 in let ws' = fromList (ws'o ++ ws'c'o)                        -- wires
 in let lbs' = lbs'c `Set.difference`
               fromList (wires2lbs ws'c'o)                     -- "closed" liberties
 in let ws'grp = let glue = foldr'
                          $ to1
                          $ foldr'
                          . flip Map.insert
                 in foldr' glue [grp'ws, grp'ws'c'o] Map.empty -- first map
 in let lbs'grp = let glue grp = flip (Set.foldr (ins0 (grp:) []))
                  in not . disjoint lbs' . snd
                   & (`List.filter` gs)
                   & List.foldr (to1 glue) Map.empty           -- second map
 in ((ws', lbs'), (ws'grp, lbs'grp))
    where game'zs = zombieless game
          ws'lbs = fmap (getWire game'zs) gs
          ws = fmap fst ws'lbs
          grp'ws = zip (fmap fst gs) ws                     -- source into first map
          gs' = gs
              & uniMultiSele2Highlight
          ws'oc = ws
                & concat
                & getWire' game'zs gs' True
          lbs'c' = wires2lbs (snd ws'oc)                    -- post removed
                 & fromList
          lbs'c = ws'lbs
                & fmap snd
                & Set.unions
                & (|++ lbs'c')
          (grp'ws'c'o, ws'c'o) = lbs'c'
                               & getGSNests game
                               & to2
                               & bimap (fmap $ bimap (fst . (gs !!))
                                                     (ws !!)
                                             . to2)         -- source into first map
                                       (concatMap (ws !!))  -- restored
getGSWire _ = ((Set.empty, Set.empty), (Map.empty, Map.empty))

-- | 'getNests' returns the indices of each nested group in
-- ^ the list of groups selected, only if the points from
-- ^ "closed" wires include (some and all) the group's liberties
getGSNests :: Game -> Set Point -> [Int]
getGSNests _ lbs | null lbs = []
getGSNests Game{meta=Meta{groupsSelected=(_,gs@(_:_))}} lbs
    = (== 2) . length . flip elemIndices gs'
    & (`List.filter` gs')
    & nub
    & least binr
    & to2
    & bimap (fmap fst) (fmap snd)
    & to1 (++)
    where gs' = gs
              & zip [0..length gs - 1]
              & List.filter (not . disjoint lbs . (snd . snd))
              & fmap (second (Set.elems . fst))
              & concatMap mm's
              & nths
          binr l = let n = length l - 1 in
                   [ (fst p, snd p')
                   | i <- [0..n], j <- [0..n], i /= j,
                          let p = l !! i, let p' = l !! j,
                          snd p == fst p'
                   ]
                   & nub
          mm's (nth, grp) = [ (nth, (fs, mm))
                            | (fs, hv) <- [(True, id), (False, swap)],
                                          let ps = (fst . coord, snd . coord),
                                          let mm = dispersion grp (hv ps)
                            ]
          inds l = let n = length l - 1
                   in [ (i, j) | i <- [0..n], j <- [i+1..n] ]
          nths l = [ if fs'i /= fs'j then []
                     else if nest mm'i mm'j then [(nthi, nthj)]
                     else if nest mm'j mm'i then [(nthj, nthi)]
                     else []
                   | (i, j) <- inds l,
                               let (nthi, (fs'i, mm'i)) = l !! i,
                               let (nthj, (fs'j, mm'j)) = l !! j
                   ]
                   & concat
          nest mm mm' = Map.foldrWithKey nested True mm
              where nested x = flip (&&) . true'fold (clamped x)
                    clamped x (miny, maxy) | x `notMember` mm' = False
                                           | otherwise =
                                               let mins = fmap fst (mm' ! x)
                                            in let maxs = fmap snd (mm' ! x)
                                            in false'fold (< miny) (List.delete max' maxs)
                                            && false'fold (> maxy) (List.delete min' mins)
                        where (min', max') = (mmin ! x, mmax ! x)
                    (mmin, mmax) = (mmmm fst List.minimum, mmmm snd List.maximum)
                    mmmm p m = let f = to1 Map.insert . second (m . fmap p)
                               in Map.foldrWithKey (curry f) Map.empty mm'
getGSNests _ _ = []

-- Wire ---------------------------------------------------------------- Util --

-- | 'dispersion' returns a list of pairs: minimum/maximum coordinates of
-- ^ contiguous stones - either horizontally or vertically dispersed;
-- ^ using a pair of projections, it finds the partition of stones
-- ^ (pseudo-iteratively), adds the rest of "singles", then ends with
-- ^ (dispersion-wise) zip'ping lists of minimum and maximum (unordered)
dispersion :: [Point] -> (Point -> Int, Point -> Int) -> Map Int [(Int,Int)]
dispersion ps hv = ps
                 & List.foldr (cons id . fst $ hv) Map.empty
                 & Map.foldrWithKey (join . snd $ hv) Map.empty
                 & (fill . fst $ hv)
                 & Map.foldrWithKey (mm's . snd $ hv) Map.empty
    where cons f tr p r = let x = tr p
                          in ins0 (f p:) [] x r
          join tr x l = let n = length l - 1 in
                      [ p|:p'|:Set.empty
                      | i <- [0..n], j <- [i+1..n],
                        let p = l !! i, let p' = l !! j,
                        adj (p, p')
                      ]
                      & Map.insert x . fmap toList . least'
              where adj = (== 1) . abs . to1 (-) . bimap1 tr
          fill tr m = ps
                    & fmap ones
                    & purifyList
                    & List.foldr (cons (:[]) tr) m
              where ones p | x `notMember` m = Just p
                           | otherwise = m ! x
                                       & List.find (p `elem`)
                                       & maybe (Just p) (const Nothing)
                        where x = tr p
          mm's tr x ll = Map.insert x (zip mins maxs)
              where mins = fmap (List.minimum . fmap tr) ll
                    maxs = fmap (List.maximum . fmap tr) ll

-- | 'uniMultiSele2Highlight' returns the 'MultiSele' pairwise 'union'ed in a 'Highlight'
uniMultiSele2Highlight :: MultiSele -> Highlight
uniMultiSele2Highlight = bimap1 Set.unions
                       . bimap (Map.map fst) (Map.map snd)
                       . to2
                       . List.foldr (to1 Map.insert) Map.empty
                       . to1 zip
                       . first ((`take` [0..]) . length)
                       . to2

-- Wire ---------------------------------------------------------------- Glue --

data Glue = Glue Int ((Int, Int) -> Int) (Point -> Int, Point -> Int)

-- | 'glueWire' returns a wire with glue
glueWire :: RenderWire -> GlueWire
glueWire ((ws, lbs), (ws'grp, lbs'grp)) = concatMap (uncurry draw) glue
    where glue = [ (gl, glueWire' ws gl)
                 |       dt <- [-1, 1],
                   (fs, hv) <- [(fst, id), (snd, swap)],
                               let ps = (fst . coord, snd . coord),
                               let gl = Glue dt fs (hv ps)
                 ]
          draw (Glue dt fs _) = Map.foldrWithKey disp []
              where tr = if fs (1, 0) == 1 then id else uncurry Point . swap . coord
                    tr' = if fs (-1, 0) == -1 then id else swap
                    disp x l r = r ++ concatMap (join x) l
                    join x (miny, maxy) = fmap wire [miny..maxy]
                        where wire y = (w, (mind, maxd))
                                  where w = bimap tr tr' (Point x y, (dt,0))
                                        grp = ws'grp ! w
                                        cvex = bool 1 0 . (`member` grp)
                                        cave p | p `member` lbs &&
                                               ( lbs'grp !? p
                                               & maybe False (grp `elem`)) = -1
                                               | otherwise = 0
                                        mind = bool 0 (cvex p'lt + cave p'lt) (miny == y)
                                        maxd = bool 0 (cvex p'gt + cave p'gt) (y == maxy)
                                        p'lt = Point (x+dt) (y-1) & tr
                                        p'gt = Point (x+dt) (y+1) & tr

-- | 'glueWire'' returns a list of pairs: minimum/maximum coordinates of
-- ^ contiguous wires - either horizontally or vertically dispersed,
-- ^ filtering either -1 or 1 delta, via 'dispersion'
glueWire' :: Set Wire -> Glue -> Map Int [(Int,Int)]
glueWire' ws (Glue dt fs hv) = dispersion (nub . wires2lbs $ ws') hv
    where ws' = toList ws
              & List.filter ((== dt) . fs . snd)

-- Wire ----------------------------------------------------------- Highlight --

-- | 'getHighlight' returns the wire for highlight matching 'Stone'
getHighlight :: Stone -> Game -> [Wire]
getHighlight _ Game{meta=Meta{highlight=(True,Empty,_)}} = []
getHighlight st game@Game{meta=Meta{highlight=(True,_,_)}}
    = uniMultiSele' st game
    & getWire game
    & fst
getHighlight _ _ = []

-- | 'getMultiSele' returns a 'MultiSele' highlight matching 'Stone'
getMultiSele :: Stone -> Game -> MultiSele
getMultiSele st Game{meta=Meta{highlight=(_,_,m'hl)},board=m}
    = (== st) . (m !) . elemAt 0 . fst
    & (`List.filter` m'hl)

-- | 'uniMultiSele'' returns the union of the 'MultiSele' highlight
uniMultiSele' :: Stone -> Game -> Highlight
uniMultiSele' st game = getMultiSele st game
                      & uniMultiSele2Highlight

-- | 'uniMultiSele' returns the union of the 'MultiSele' highlight as lists
uniMultiSele :: Stone -> Game -> ([Point], [Point])
uniMultiSele = curry
             $ bimap1 toList
             . to1 uniMultiSele'

-- Highlight -------------------------------------------------------------------

-- | 'toggleHighlight' toggles highlighting group(s) of stones
toggleHighlight :: Game -> Game
toggleHighlight game@Game{meta=md@Meta{highlight=(ehl,_,_)}} =
    game{meta=md{highlight=(not ehl,Empty,[])}}

-- | 'cancelHighlight' cancels highlighting along zombies or outside the board
-- ^ only if there is no possibility of not changing the stone color
cancelHighlight :: Game -> Point -> Game
cancelHighlight game@Game{meta=md,boardSize=s} p
    | hlst == stone &&
      alongBorder 0 (s+1) p &&
      ((isJust $ List.find (member p' . fst) m'hl) ||
      (not . null $ ps)) = game
    | otherwise = game{meta=md{highlight=(ehl,Empty,[])}}
    where stone = if st' /= Empty && st /= st' then st'
                  else if st /= Empty && st' == Empty then st
                  else Empty
          st = seekBoard' (zombies' zs) p
          st' = seekBoard' (zombies' sz) p
          m' = getZombies game & purify
          p' = zombiesBorder s p
          ps = p
             & getAdj'
             & List.filter (to1 (&&)
                           . bimap (alongBorder 0 (s+1))
                                   ((== stone) . seekBoard' m')
                           . to2)
          Meta{highlight=(ehl,hlst,m'hl),deadZombies=zs,liveZombies=sz} = md

-- Highlight ---------------------------------------------------------- Group --

-- | 'reHighlightAt' sets the pending op-code (either "hl" or "gs") for highlighting
-- at 'Point' - and just that -, depending if it is already pending or enabled,
-- while also considering the list of groups selected ("gs" pending op-code)
reHighlightAt ::  Point -> Game -> Game
reHighlightAt p game@Game{meta=md} =
    pendingText op . pipe . findHighlight p $ game
    where op = bool "gs" (bool "" "hl" ehl) (null gs)
          pipe = bool id (clearPending True) (maybe False ((`elem` ["hl", "gs"]) . take 2) pl)
          Meta{pending=pl,highlight=(ehl,_,_),groupsSelected=gs} = md

-- | 'findHighlight' finds at most one 'MultiSele' highlight from stone 'Point'
findHighlight :: Point -> Game -> Game
findHighlight _ game@Game{meta=Meta{highlight=(False,_,_)}} = game
findHighlight point game@Game{meta=md,board=m} =
    game{meta=md{highlight=(True,st,[(grp,lbs)])}}
    where st = seekBoard' m point
          (grp, lbs) = findGroup game st point

-- Highlight ---------------------------------------------------------- Point --

-- | 'rePointHighlightAt' sets the pending op-code (either "hl" or "gs") for highlighting'
-- at 'Point' - and just that -, depending if it is already pending or enabled,
-- while also considering the list of groups selected ("gs" pending op-code)
rePointHighlightAt :: Point -> Game -> Game
rePointHighlightAt p game@Game{meta=md} =
    pendingText op . pipe . findPointHighlight p $ game
    where op = bool "gs" (bool "" "hl" ehl) (null gs)
          pipe = bool id (clearPending True) (maybe False ((`elem` ["hl", "gs"]) . take 2) pl)
          Meta{pending=pl,highlight=(ehl,_,_),groupsSelected=gs} = md

-- | 'findPointHighlight' finds at most 4 'MultiSele' highlights from liberty 'Point'
findPointHighlight :: Point -> Game -> Game
findPointHighlight _ game@Game{meta=Meta{highlight=(False,_,_)}} = game
findPointHighlight point game@Game{meta=md,board=m,boardSize=s} =
    game{meta=md{highlight=(True,Ko,m'hl)}}
    where m'hl = getAdj' point
               & List.foldr uniq []
          uniq p r
              | boardPoint s p |-> st == Empty ||
                (isJust $ List.find (member p . fst) r) = r
              | otherwise = findGroup game st p:r
              where st = seekBoard' m p

-- Zombies ---------------------------------------------------------------------

-- | 'zombieless' returns a game without live or dead zombies
zombieless :: Game -> Game
zombieless game@Game{meta=md} =
    game{meta=md{deadZombies=Nothing,liveZombies=Nothing}}

-- | 'zombies'' converts a possibly Nothing 'Maybe' into a possibly empty map
zombies' :: Maybe (Map Point Stone) -> Map Point Stone
zombies' = fromMaybe Map.empty

-- | 'zombies''' converts a possibly empty map into a possibly Nothing 'Maybe'
zombies'' :: Map Point Stone -> Maybe (Map Point Stone)
zombies'' zs = bool (Just zs) Nothing (null zs)

-- Zombies ----------------------------------------------------------- Toggle --

-- | 'toggleZombies' toggles dead zombies
toggleZombies :: Game -> Game
toggleZombies game@Game{meta=md@Meta{deadZombies=Nothing}} =
    mirrorZombies game{meta=md{showHelp=False}}
toggleZombies game = game'{meta=md'{liveZombies=sz}}
                   & mergeBornZombies
    where game'@Game{meta=md'} = zombieless game
          sz = getLiveZombies game
             & zombies''

-- | 'sweepLiveZombies' removes live zombies from (live and dead) zombies
sweepLiveZombies :: Game -> Game
sweepLiveZombies game@Game{meta=md@Meta{deadZombies=zs,liveZombies=Just _}} =
    mergeBornZombies game{meta=md{liveZombies=sz}}
    where ps = getOverlapDeadZombies game
             & fromList
          sz = zs
             & zombies'
             & (`Map.restrictKeys` ps)
             & zombies''
sweepLiveZombies game = game

-- | 'toggleDeadZombies' toggles a dead zombie
toggleDeadZombies :: Bool -> Point -> Game -> Game
toggleDeadZombies remove pt game@Game{meta=md@Meta{deadZombies=Just zs}}
    | st /= Empty &&
      seekBoard' sz' pt == st = game{meta=md{liveZombies=sz'rem}}
                              & if pt `notMember` bz || bz ! pt /= st then id
                                else toggleDeadZombies False pt
    | remove |->
      pt `notMember` bz = game{meta=md{liveZombies=sz'add (bool st' st remove)}}
    | otherwise = game
    where st = seekBoard' zs pt
          st' = getOppositeStone st
          sz' = zombies' sz
          sz'rem = removePiece sz' pt & zombies''
          sz'add st'' = addPiece st'' pt sz' & zombies''
          Meta{bornZombies=bz,liveZombies=sz} = md
toggleDeadZombies _ _ game = game

-- | 'toggleLiveZombies' toggles a live zombie
toggleLiveZombies :: Bool -> Point -> Game -> Game
toggleLiveZombies remove pt game@Game{meta=md}
    | st == Empty = game{meta=md{liveZombies=sz'add}}
    | otherwise = game{meta=md{liveZombies=bool sz'tgl sz'rem remove}}
    where st = seekBoard' sz' pt
          st' = getOppositeStone st
          sz' = zombies' sz
          sz'add = addPiece stone pt sz' & zombies''
          sz'tgl = addPiece st' pt sz' & zombies''
          sz'rem = removePiece sz' pt & zombies''
          stone = getTurnStoneFromMove lm m
          Meta{liveZombies=sz} = md
          Game{maxMoves=m,lastMove=lm} = game

-- | 'toggleZombiesSpan' toggles multiple live/dead zombies that span from 'Point'
toggleZombiesSpan :: Point -> Stone -> Game -> Game
toggleZombiesSpan pt st game@Game{meta=md} = game{meta=md{liveZombies=sz''}}
    where Meta{bornZombies=bz,liveZombies=sz} = md
          st' = getOppositeStone st
          sz' = zombies' sz
          ps = spanZombies game pt
          sz'zs = getOverlapLiveZombies game
          ps'rem = sz'zs
                 & intersect ps
          ps'tgl = getLiveZombies game
                 & keys
                 & (\\ sz'zs)
                 & intersect ps
          ps'add = getDeadZombies game
                 & Map.union bz
                 & keys
                 & intersect ps
          sz'' = sz'
               & foldl' removePiece ps'rem
               & foldr' (addPiece st') (ps'tgl ++ ps'add)
               & zombies''

-- Zombies -------------------------------------------------------------- Get --

-- | 'getDeadZombies' returns the dead zombies
getDeadZombies :: Game -> Map Point Stone
getDeadZombies game@Game{meta=Meta{deadZombies=Just zs}}
    = getOverlapDeadZombies game ++
      getOverlapLiveZombies game
    & fromList
    & Map.withoutKeys zs
getDeadZombies _ = Map.empty

-- | 'getOverlapDeadZombies' returns the points of uncounted dead zombies
getOverlapDeadZombies :: Game -> [Point]
getOverlapDeadZombies Game{meta=Meta{deadZombies=Just zs,liveZombies=Just sz}}
    = Map.intersectionWith (==) zs sz
    & Map.filter id
    & keys
getOverlapDeadZombies _ = []

-- | 'getOverlapLiveZombies'' returns the points of dead zombies that are live
getOverlapLiveZombies :: Game -> [Point]
getOverlapLiveZombies Game{meta=Meta{deadZombies=Just zs,liveZombies=Just sz}}
    = Map.intersectionWith (/=) sz zs
    & Map.filter id
    & keys
getOverlapLiveZombies _ = []

-- | 'getLiveZombies' returns the live zombies
getLiveZombies :: Game -> Map Point Stone
getLiveZombies game@Game{meta=Meta{liveZombies=Just sz}}
    = getOverlapDeadZombies game
    & fromList
    & Map.withoutKeys sz
getLiveZombies _ = Map.empty

-- | 'getZombies' returns the union of live and dead and born zombies
getZombies :: Game -> Maybe (Map Point Stone)
getZombies game@Game{meta=Meta{bornZombies=bz}}
    = getLiveZombies game
      `Map.union`
      getDeadZombies game
      `Map.union`
      bz
    & zombies''

-- | 'getZombies'' returns the union of live and dead zombies
getZombies' :: Game -> Maybe (Map Point Stone)
getZombies' game = getLiveZombies game
                   `Map.union`
                   getDeadZombies game
                 & zombies''

-- Zombies ----------------------------------------------------------- Mirror --

-- | 'zombiesLiveGroups' is only used from 'mirrorZombies' to overlap other zombies
zombiesLiveGroups :: Game
                  -> Set Point
                  -> Map Point Stone
zombiesLiveGroups game@Game{maxMoves=1,board=m,boardSize=s} cs =
    let ds = (Black, White)
           & bimap1 only
           & bimap1 (to1 some)
 in let os = ds
           & bimap1 fst
           & to1 Set.union
 in if null os
    then Map.empty
    else let pt = elemAt 0 os in
             ds
           & bimap1 ((:[]) . snd)
           & to1 (++)
           & purifyList
           & List.foldr (to1 ($)
                        . bimap (foldr' . addPiece . fst . snd)
                                (to1 (zombiesLiveGroups' pt))
                        . to2) Map.empty
    where only stone = game { board = cs
                                    & Map.withoutKeys m
                                    & Map.filter (== stone) }
                     & (stone,)
          some stone game'@Game{board=m'} =
              let zs = boardBorders 0 (s+1)
                     & concat
                     & nub
                     & List.foldr (addPiece stone) Map.empty
           in let bs = m'
                     & Map.filterWithKey (flip . const $ alongBorder 1 s)
                     & keys
           in if null bs
              then let os = m'
                          & keysSet
                   in (os, Nothing)
              else let gs'lbs'zs = bs
                                 & head
                                 & findGroup game' stone
                                 & flip (mostZombies game' stone) zs
                in let gs = gs'lbs'zs
                          & fst . fst
                in let os = gs
                          & Map.withoutKeys m'
                          & keysSet
                   in (os, Just (game', (stone, (bs, gs'lbs'zs, gs))))
zombiesLiveGroups _ _ = Map.empty

-- | 'zombiesLiveGroups'' occurs from 'zombiesLiveGroups''
zombiesLiveGroups' :: Point
                   -> Game
                   -> (Stone, ([Point], (Highlight, Set Point), Set Point))
                   -> [Point]
zombiesLiveGroups' pt game@Game{board=m,boardSize=s} (stone, (bs, gs'lbs'zs, gs))
    = gs'hs
    & List.filter ((> 1) . length . snd)
    & to2
    & bimap (fmap fst) (fmap (Set.unions . snd))
    & bimap1 Set.unions
    & to1 Set.union
    & Set.filter (alongBorder 1 s)
    & toList
    & concatMap getAdj'
    & nub
    & List.filter (alongBorder 0 (s+1))
    where game'@Game{board=m'} = game { board = pt `Set.delete` gs
                                              & Map.restrictKeys m }
          hs = gs'lbs'zs
             & second (second near
                      . to2)
             & to2
             & bimap (to1 (|++)
                     . bimap1 fst)
                     (to1 (|++)
                     . bimap1 snd)
             & flip (getHoles game' m') 1
             & snd
             & List.filter (not . member pt)
          near = Set.filter ((== Empty) . seekBoard' m)
               . Set.map (zombiesBorder s)
               . (`Set.difference` zombiesCorners s)
          uniq p r
              | isJust $ List.find (member p . fst) r = r
              | otherwise = findGroup game' stone p:r
          join l = let n = length l - 1 in
                 [ (grp'i |++ grp'j, lbs'i |++ lbs'j)
                 | i <- [0..n], j <- [i+1..n],
                   let (grp'i, lbs'i) = l !! i,
                   let (grp'j, lbs'j) = l !! j,
                   isJust $ List.find (hole lbs'i lbs'j) hs
                 ]
          hole lbs'i lbs'j = to1 (&&)
                           . bimap (not . disjoint lbs'i)
                                   (not . disjoint lbs'j)
                           . to2
          gs'lbs = bs
                 & List.foldr uniq []
          gs'lbs' = gs'lbs
                  & least join
          gs' = gs'lbs'
              & fmap fst
          gs'hs = gs'lbs
                & List.foldr ones []
                & (gs'lbs' ++)
                & List.foldr (flip foldr' hs . acch) Map.empty
                & assocs
                & to2
                & bimap (fmap (fst . fst))
                        (fmap snd)
                & to1 zip
          ones (grp,lbs) = gs'
                         & List.find (not . disjoint grp)
                         & isJust
                         & bool ((grp,lbs):) id
          acch (grp,lbs) ps = booly (lbs `disjoint` ps)
                            . first (ins0 (ps:) [] (grp,lbs))
                            . to2

-- | 'clearLiveZombies' is only used from 'mirrorZombies' to
-- ^ remove the live zombies for uncounted dead
clearLiveZombies :: Game -> Game
clearLiveZombies game@Game{meta=md@Meta{liveZombies=Just sz}} =
    let sz' = getOverlapDeadZombies game
            & fromList
            & Map.withoutKeys sz
            & zombies''
     in game{meta=md{liveZombies=sz'}}
clearLiveZombies game = game

-- | 'mergeLiveZombies' is only used from 'mirrorZombies' to
-- ^ "preserve" the live zombies when dead zombies change:
-- ^ a live zombie is only replaced by a dead zombie of the same color
mergeLiveZombies :: Game -> Game
mergeLiveZombies game@Game{meta=md@Meta{deadZombies=zs}} =
    let sz'' = zs'sz `Map.union` sz'zs
             & zombies''
    in game{meta=md{liveZombies=sz''}}
    where zs' = zombies' zs
          sz' = getLiveZombies game
          zs'sz = Map.intersectionWith (/=) sz' zs'
                & Map.filter id
                & keysSet
                & Map.restrictKeys sz'
          sz'zs = zs'
                & keysSet
                & Map.withoutKeys sz'

-- | 'mergeBornZombies' fills gaps between same-color (live or dead) zombies
mergeBornZombies :: Game -> Game
mergeBornZombies game@Game{meta=md,boardSize=s} =
    if null zs' then game{meta=md{bornZombies=Map.empty}}
    else let bz = [ ((id     , snd), (==0)   . fst) -- left  v
                  , ((id     , fst), (==s+1) . snd) -- down  >
                  , ((reverse, snd), (==s+1) . fst) -- right ^
                  , ((reverse, fst), (==0)   . snd) -- up    <
                  ]
                & concatMap (to1 (to1 wrap))
                & nub -- corners
                & to2
                & bimap (first fst . to2 . head)
                        (bimap (tail . cycle)
                               ((:[]) . head)
                        . to2)
                & to1 (to1 born)
                & List.foldr (to1 Map.insert) Map.empty -- "Map.fromList"
                & (`Map.difference` zs')
      in game{meta=md{bornZombies=bz}}
    where zs' = getZombies' game
              & zombies'
          wrap asc onc ofc = zs'
                           & assocs
                           & List.filter (ofc . coord . fst)
                           & asc . List.sortOn (onc . coord . fst)
          born p (pt',st') ((pt,st):ps, r)
              = r
              & bool ((pt,st):) fill (st == st')
              & bool (born p (pt,st) . (ps,)) id (p == pt)
              where fill = pt' -- previous
                         & next
                         & (`drag` [])
                         & fmap (,st)
                         & (++)
                    drag = curry
                         $ to1 ($)
                         . bimap (to1 booly
                                 . bimap (== pt)
                                         (bimap (drag . next)
                                                (flip const)
                                         . to2)
                                 . to2
                                 . fst)
                                 (to1 (:))
                         . to2
          next (Point 0 0) = Point 0 1
          next (Point 0 y) | y == s+1 = Point 1 (s+1)
          next (Point x y) | x == s+1 && y == s+1 = Point (s+1) s
          next (Point x 0) | x == s+1 = Point s 0
          next (Point 0 y) = Point 0 (y+1)                -- left  v
          next (Point x y) | y == s+1 = Point (x+1) (s+1) -- down  >
          next (Point x y) | x == s+1 = Point (s+1) (y-1) -- right ^
          next (Point x 0) = Point (x-1) 0                -- up    <

-- Zombies ----------------------------------------------------- Transitivity --

-- | 'spanZombies'' finds the zombies group of same color as non-corner 'Point'
spanZombies' :: Int
             -> Map Point Stone
             -> Point
             -> Set Point -> Set Point
spanZombies' s zs pt@(Point x y) fs
    | pt `member` fs = fs
    | x == 0 || x == s+1 = join (take 2) cx
    | y == 0 || y == s+1 = join (drop 2) cy
    | otherwise = fs
    where st = seekBoard' zs pt
          join t c = ps
                   & to2
                   & bimap (|++ fs)
                           (Set.filter ((== st) . seekBoard' zs . c)
                           . intersection cs)
                   & to1 (List.foldr turn)
              where ps = loop fs [pt]
                       & (`Set.difference` fs)
                    loop r [] = r -- empy queue
                    loop r (p:q) | p `member` r = loop r q
                                 | otherwise = q ++ get' p
                                             & loop (p|:r)
                    get' = List.filter ((== st) . seekBoard' zs) . t . getAdj'
                    turn p = spanZombies' s zs (c p) . (p|:)
          cs = zombiesCorners s
          cx (Point x' y') = Point (d x') y'
          cy (Point x' y') = Point x' (d y')
          d n = n + bool (-1) 1 (n == 0)

-- | 'spanZombies' finds the zombies group of same color as 'Point'
-- corners as well
spanZombies :: Game -> Point -> [Point]
spanZombies game@Game{boardSize=s} p@(Point x y)
    | p `notElem` zombiesCorners s = spanZombies' s zs p Set.empty
                                   & toList
    | otherwise = singleton p
                & bool id (spanZombies' s zs cx) (st == seekBoard' zs cx)
                & bool id (spanZombies' s zs cy) (st == seekBoard' zs cy)
                & toList
    where zs = getZombies game & zombies'
          st = seekBoard' zs p
          cx = Point (d x) y
          cy = Point x (d y)
          d n = n + bool (-1) 1 (n == 0)

-- | 'mostZombies' finds any border 'Stone' adjacent to any zombie
-- ^ transitively by the "friend through zombies" equivalence
mostZombies :: Game
            -> Stone
            -> Highlight
            -> Map Point Stone
            -> (Highlight, Set Point)
mostZombies game@Game{boardSize=s} st (grp, lbs) zs
    = (grp, lbs)
    & (, Set.empty)
    & mostZombies' game st zs (Set.filter (alongBorder 1 s) grp)

-- | 'mostZombies'' occurs from 'mostZombies'
mostZombies' :: Game
             -> Stone
             -> Map Point Stone
             -> Set Point
             -> (Highlight, Set Point)
             -> (Highlight, Set Point)
mostZombies' game@Game{board=m,boardSize=s} st zs fr'zs ((grp, lbs), fs)
    | null fr'zs = ((grp, lbs), fs)
    | otherwise = mostZombies' game st zs fr'zs'
                  ((grp |++ grp', lbs |++ lbs'), fs |++ zs'grp)
    where (zs'grp, zs'fr'grp) = pipe fr'zs
          (fr'zs', (grp', lbs')) = zs'fr'grp
                                 & (`Set.difference` grp)
                                 & toList
                                 & fmap (findGroup game st)
                                 & uniMultiSele2Highlight
                                 & to2
                                 & first (Set.filter (alongBorder 1 s)
                                         . (`Set.difference` grp)
                                         . fst)
          pipe = second near
               . to2
               . List.foldl (flip (spanZombies' s zs)) fs
               . List.filter (to1 (&&)
                             . bimap (to1 (&&)
                                     . bimap (not . (`notMember` zs))
                                             (alongBorder 0 (s+1))
                                     . to2)
                                     ((== st) . (zs !))
                             . to2)
               . nub
               . concatMap getAdj'
               . toList
          near = Set.filter ((== st) . seekBoard' m)
               . Set.map (zombiesBorder s)

-- Zombies ----------------------------------------------------------- Mirror --

type Propagation = (
  Set Point,                -- bolted stones
  Int -> Int -> Bool,       -- less/greater than
  (Int, Int) -> (Int, Int), -- board coord to (key, value)
  Point -> Point            -- mirror to board
                   )

type Phase = (Int, Stone)
type Wave = Map Int Phase
data Mirror = Mirror Wave Wave Game -- "depth of the image"

type Reflection = [(Int, Stone)]

-- | 'thrill' "propagates" updating 'Wave' at 'Int'
thrill :: Int -> Phase -> Wave -> Wave
thrill x p = Map.insert x p . Map.delete x

-- | 'propagation' returns 'Point' translations for 0-up/1-down/2-left/3-right
-- ^ less/greater than are used in the initial (projection) wave
propagation :: (Int -> Set Point) -> Game -> Int -> Propagation
propagation mb Game{boardSize=s} n =
    case n of
      0 -> (bs, (<), id,   \(Point x y) -> Point ((s+1)-x) y        )
      1 -> (bs, (>), id,   \(Point x y) -> Point x ((s+1)-y)        )
      2 -> (bs, (<), swap, \(Point x y) -> Point y x                )
      _ -> (bs, (>), swap, \(Point x y) -> Point ((s+1)-y) ((s+1)-x))
    where bs = mb n

-- Mirror ------------------------------------------------------------ Bolted --

-- | 'mirrorBolted'' finds the border points for which the access of
-- ^ group(s) of stones is "open" (not "closed" by other stones)
-- ^ returns a map of "open" points for each border side and a set of
-- ^ the points of the stones which are entirely "closed"
mirrorBolted' :: Game -> Map Point Int -> (Map Int (Set Point), Set Point)
mirrorBolted' game@Game{meta=Meta{pointsKo'=kos'},board=m,boardSize=s} bs =
    let gs = keys m \\ (getPointsKo game ++ kos')
           & List.foldl (curry group) []
     in gs
      & to2
      & bimap ((Map.empty,)                                 -- already "closed" points to 0-3
              . List.foldr (to1 (foldr' . accpt)) Map.empty -- "open" points
              . fmap (second open' . to2))                  -- groups already "open"
              (foldr' (mirrorWhirl game bs))
      & to1 (&)
      & snd
      & to2
      & first (([0..3] \\) . keys)
      & to1 (foldr' (`Map.insert` Set.empty))
      & to2
      & second (to1 Set.difference
               . bimap Set.unions Set.unions
               . (gs,))
    where group = to1 ($)
                . first (flip (:))
                . second (fst
                         . to1 (findGroup game)
                         . first (m !)
                         . to2)
          open' = toList
                . Set.map (bs !)
                . Set.filter (alongBorder 1 s)
          accpt = (`ins0` Set.empty) . (|++)

-- | 'mirrorWhirl' finds the borders that are "open" or "closed" for
-- ^ a group of stones
mirrorWhirl :: Game
            -> Map Point Int        -- border point to 0/1/2/3
            -> Set Point            -- group
            -> (Map Point (Set Int) -- already "closed" points to 0-3
               ,Map Int (Set Point))
            -> (Map Point (Set Int)
               ,Map Int (Set Point))
mirrorWhirl Game{meta=md,board=m,boardSize=s} bs grp o'r =
    let (ps, (oc, r)) = whirl
 in let ns = oc
           & bimap1 (fromMaybe Set.empty . (!? point))
           & to1 Set.union
           & Set.difference (fromList [0..3])
 in let cs = oc
           & bimap1 keysSet
           & to1 Set.union
           & Set.difference ps
 in oc
  & snd
  & to2
  & first (flip (Set.foldr (ins0 (ns|++) Set.empty)) cs)
  & booly (null ns)
  & (,r)
    where Meta{spiral=sp} = md
          point = elemAt 0 grp
          o'c'r = bimap ((Map.empty,) . fst) -- "open" points to 0-3
                        snd
                . to2
                $ o'r
          reach = to1 (&&)
                . bimap (not . (point `notMember`))
                        ((== 4) . Set.size . (! point))
                . to2
          whirl = magic point s 0
                & spiralFold' True stop path o'c'r sp
              where stop ((o, c), _) = reach o -- entirely "open"
                                    || reach c -- entirely "closed"
                    path (pt, ps, (oc@(o, c), r))
                        | not $ pt `notMember` o = -- already "open"
                           let ns'o = ns o
                           in ns'o
                            & fmap (flip accns)
                            & foldr' (($ grp|++ps) . (flip . Set.foldr))
                            & (,foldr' accpt ns'o)
                            & ((id, id),)
                            & booly (null ns'o)
                            & bimap ($ o) ($ r)
                            & first (,c)
                            & (Just True,)
                        | not $ pt `notMember` c = -- already "closed"
                            let ns'c = ns c
                            in ns'c
                             & fmap (flip accns)
                             & foldr' (($ grp|++ps) . (flip . Set.foldr))
                             & (id,)
                             & booly (null ns'c)
                             & ($ c)
                             & (,r) . (o,)
                             & (Just False,)
                        | pt `member` grp = (Just True, (oc, r))
                        | st == Empty = pt|:(grp|++ps)
                                      & flip (Set.foldr (`accns` n))
                                      & (,accpt n)
                                      & ((id, id),)
                                      & booly (alongBorder 1 s pt)
                                      & bimap ($ o) ($ r)
                                      & first (,c)
                                      & (Just True,)
                        | otherwise = (Just False, (oc, r))
                        where st = seekBoard' m pt
                              n = bs ! pt
                              accns p = to1 (`ins0` Set.empty) . (,p) . (|:)
                              accpt = ins0 (grp|++) Set.empty
                              ns :: Map Point (Set Int) -> [Int]
                              ns = toList
                                 . to1 ($)
                                 . bimap (Set.difference . (! pt))
                                         (fromMaybe Set.empty . (!? point))
                                 . to2

-- | 'mirrorBolted' finds for each stone the border sides
-- ^ (0-up, 1-down, 2-left, 3-right) to which there is no
-- ^ transitive adjacency through empty points
mirrorBolted :: Game -> (Set Point, Map Int (Set Point))
mirrorBolted game@Game{boardSize=s} =
    let os = os'
           & Set.unions
    in Map.empty
     & foldr' (to1 ($)
              . bimap Map.insert
                      ((cs|++) . Set.difference os . (os' !))
              . to2)
              [0..3]
     & (cs,)
    where (os', cs) = let ns = [ head, head . tail,
                                 last . init, last ]
                   in let bs = boardBorders 1 s
                   in let borders n = bs
                                    & fmap (ns !! n)
                                    & foldr' (`Map.insert` n)
                       in Map.empty
                        & foldr' borders [0..3]
                        & mirrorBolted' game

-- Mirror ----------------------------------------------------------- Zombies --

-- | 'mirrorZombies' finds the dead zombies, merges the live zombies via
-- ^ 'mergeLiveZombies' and fills gaps between live or dead zombies via
-- ^ 'mergeBornZombies'
mirrorZombies :: Game -> Game
mirrorZombies game@Game{board=m,boardSize=s} =
    let zs = ps
           & fmap (mirrorInitialization s m)
           & zip ps
           & fmap (to1 (mirrorPropagation m))
           & zip ps
           & fmap (to1 mirrorReflection)
           & mirrorCompletion s
 in let zs' = zombiesLiveGroups game cs
            & Map.union zs
            & zombies''
 in let game'@Game{meta=md'} = clearLiveZombies game
     in game'{meta=md'{deadZombies=zs'}}
      & mergeLiveZombies
      & mergeBornZombies
    where (cs, mb) = mirrorBolted game
          ps = [0..3]
             & fmap (propagation (mb !) game)

-- Mirror -------------------------------------------------------- Completion --

-- | 'mirrorCompletion' returns the four sides zombies stones
mirrorCompletion :: Int -> [Reflection] -> Map Point Stone
mirrorCompletion s bs
    = fmap comp [ (`Point` 0), (`Point` (s+1)), Point 0, Point (s+1) ]
    & (`zip` bs)
    & concatMap (to1 fmap)
    & List.foldr (to1 addPiece) Map.empty
    where comp :: (Int -> Point) -> (Int, Stone) -> (Stone, Point)
          comp pf = swap . to1 ((,) . pf)

-- Mirror -------------------------------------------------------- Reflection --

-- | 'mirrorReflection' returns the stones at border points
mirrorReflection :: Propagation -> Mirror -> Reflection
mirrorReflection (_, _, tr', tr) (Mirror _ w Game{board=m})
    = w
    & keys
    & fmap (to2 . (`Point` 1))
    & fmap (bimap (fst . tr' . coord . tr)
                  (m !))

-- Mirror ----------------------------------------------------------- Initial --

-- | 'mirrorInitialization' returns a 'Mirror' after the initial projections
mirrorInitialization :: Int -> Map Point Stone -> Propagation -> Mirror
mirrorInitialization s m (bs, lt, tr', tr) =
    List.foldr proj none [1..s]
    where none = Mirror Map.empty Map.empty (createGame' s)
          proj x = fmap (Point x) [1..s]
                 & foldr' prox
              where prox pt r@(Mirror w' w game)
                        | st' == Empty || pt' `member` bs || not less = r
                        | otherwise =
                            let w'1 = thrill x' (y', st') w'
                         in let w1 = thrill x (1, st') w
                         in let game1 = putPiece st' [Point x 1] game
                         in Mirror w'1 w1 game1
                        where pt' = tr pt
                              st' = seekBoard' m pt'
                              (x', y') = tr' . coord $ pt'
                              less = maybe True (lt y' . fst) (w' !? x')

-- Mirror ----------------------------------------------- Propagation&Tossing --

-- | 'mirrorPropagation' propagates mirror waves while tossing captured stones
mirrorPropagation :: Map Point Stone -> Propagation -> Mirror -> Mirror
mirrorPropagation m pr@(bs, _, tr', tr) mr@(Mirror _ _ Game{boardSize=s}) =
    let mr1@(Mirror _ w _) = foldl' wave [1..s] mr
    in if true'fold ((==s) . fst) w then mr1
       else mirrorPropagation m pr mr1
    where wave r@(Mirror w' w _) x =
            case w !? x of
              Nothing                 -> r
              Just (y, _) | y == s    -> r
                          | otherwise -> Point x y
                                       & tr' . coord . tr
                                       & fst
                                       & (w' !?)
                                       & fmap (getOppositeStone . snd)
                                       & maybe r (wave' r (Point x y))
          wave' r pt@(Point x y) st' = wave1 r pt (rim - y)
              where rim = (True, s)
                        & foldl' loop [y+1..s]
                        & snd
                    loop (False, y') _ = (False, y')
                    loop _ y' | pt' `member` bs ||
                                seekBoard' m pt' /= st' = (True, s)
                              | otherwise = (False, y')
                        where pt' = tr $ Point x y'
          wave1 (Mirror w' w game) (Point x y) dy =
              let w'1 = thrill x' (y', st) w' in
              let w1 = thrill x (y+dy, st) w in
              let game1 = game
                        & putPiece (snd (w ! x)) (fmap (Point x) [y+1..y+dy-1])
                        & bool (putPiece st [pt]) id (st == Empty)
           in Mirror w'1 w1 (toss (Point s 1) game1)
               where pt = Point x (y+dy)
                     pt' = tr pt
                     (x', y') = tr' . coord $ pt'
                     st = seekBoard' m pt'
          toss pt@(Point x _) game1@Game{board=m1}
              | x == 0 = game1
              | otherwise = getOppositeStone st
                          & (`putPiece` ps)
                          & (,id)
                          & booly (mod (length ps) s*s == 0)
                          & ($ game1)
                          & to2
                          & bimap (toss (Point s 1))
                                  (toss pt')
                          & booly (mod (length ps) s*s == 0)
              where st = seekBoard' m1 pt
                    pt' = Point (x-1) 1
                    ps = removeDead pt st game1
                       & (,[])
                       & booly (st == Empty)

-- FollowUp --------------------------------------------------------------------

-- | 'best' returns the highest priority followup
-- (pairs being the first)
best :: Game -> IO (Maybe (Bool, ([Point], Bool)))
best game = do
    kos <- fuPointsKo game
    let n = length kos
    return case () of
             _ | tc > length kos -> let fup = pipe n ll
                                 in Just (n == 0, (fup:kos, Map.size ll > n + 1))
             _                   -> Nothing
    where Game{meta=Meta{followup=fu}} = game
          Numbering{labelling=ll,totalCount=tc} = fu
          pipe n = head . keys . Map.filter (high n)
          high n l = case l of
                       Letter c -> n + 1 == ord c - ord 'a' + 1
                       Number i -> n + 1 == i

-- | 'rest' continues 'Game' with the remaining followups in 'Numbering'
rest :: Numbering -> [Point] -> Game -> Game
rest fu ps game@Game{meta=md}
    | tc < n = game
    | otherwise = game { meta = md { followup = fu { labelling = ps
                                                               & fromList
                                                               & Map.withoutKeys ll
                                                               & Map.map decr
                                                   , totalCount = tc - n
                                                   }
                                   }
                       }
    where Numbering{labelling=ll,totalCount=tc} = fu
          n = length ps
          decr l = case l of
                     Letter c -> Letter (chr (ord c - n))
                     Number i -> Number (i - n)

-- | 'followupAt' either adds the succeeding followup label, or
-- swaps with the succeeding/preceding, or removes the greatest
followupAt :: Bool -> Point -> Game -> IO Game
followupAt _ _ game@Game{meta=Meta{followup=Numbering{enabled=False}}} = return game
followupAt f pt game@Game{meta=md} = do
    kos <- fuPointsKo game
    let n = length kos
    if tc > 0 && tc == n
    then return game
    else do
      let n' = tc + n
      let m = bool tc (1 + n) f
      return case ll !? pt of
               Just (Letter c) | not f && (tc > 1 || n' == 1) &&
                                 ord c - ord 'a' + 1 == n'
                                          -> remove
               Just (Number i) | not f && (tc > 1 || n' == 1) &&
                                 i == n'  -> remove
               Just l@(Letter c) | gt (ord c - ord 'a' + 1) m
                                          -> toggle l (Letter (chr (ord c + d)))
               Just l@(Number i) | gt i m -> toggle l (Number (i + d))
               Just _                     -> game
               _                          -> append
    where Meta{followup=fu@Numbering{labelling=ll,totalCount=tc}} = md
          gt = bool (<) (>) f
          d = bool 1 (-1) f
          append = game{meta=md{followup=incNumbering fu pt}}
          toggle l = game' tc
                   . to1 (`addPiece` pt)
                   . bimap (to1 (!))
                           (($ ll) . addPiece l . snd)
                   . to2
                   . second (head . keys)
                   . to2
                   . (`Map.filter` ll)
                   . (==)
          remove = removePiece ll pt
                 & game' (tc-1)
          game' tc' ll' = game{meta=md{followup=fu{labelling=ll',totalCount=tc'}}}

-- Groups -------------------------------------------------------------- Eyes --

-- | 'liveGroupCapture' finds the liberties of the group that has an eye
-- ^ at 'Point' and other hole(s)
liveGroupCapture :: Game -> Point -> Int -> [Point]
liveGroupCapture game@(Game _ mm _ _ lm s _ _ _) p@(Point x y) moves
    | mm == 1 = []
    | x < 1 || x > s || y < 1 || y > s = []
    | seekBoard game p /= Empty = [] -- If point is not empty
    | otherwise = let ((grp, lbs), lbs') = getOppositeStone st
                                         & findEye game p
               in if null grp then [] -- Empty adjacent / more groups
                  else
                  let hs = ((grp, lbs), lbs')
                         & getHoles' game
               in if length hs == 1 -- only eye
                  then []
                  else hs
                     & Set.unions
                     & (lbs |++)
                     & Set.delete p
                     & toList
                     & (p:)
                     & to2
                     & bimap ([],)
                             (booly . clamp' 1 moves)
                     & to1 (&)
    where st = getTurnStoneFromMove lm mm

-- | 'findEye' finds an eye at 'Point' of one and the same group of 'Stone's
-- ^ and returns via 'findGroup''
findEye :: Game -> Point -> Stone -> (Highlight, Set Point)
findEye game@Game{boardSize=s} p st = getAdj' p
                                    & List.foldr eye' (True, hl0)
                                    & snd
    where eye' _ (False, _) = (False, hl0)
          eye' p' (_, r@((grp, _), _))
              | boardPoint s p' |-> p' `member` grp = (True, r)
              | null grp |-> seekBoard game p' /= st = (False, hl0)
              | otherwise = hl0
                          & findGroup' game st p'
                          & (True,)

-- Groups --------------------------------------------------------- Liberties --

-- | 'findGroup' finds liberties (Ko included) of- and group of- stones in the 'Game'
-- starting from 'Point' and going in all directions
findGroup :: Game -> Stone -> Point -> Highlight
findGroup game p stone = hl0
                       & findGroup' game p stone
                       & fst

-- | 'findGroup'' occurs from 'findGroup'
findGroup' :: Game -> Stone -> Point
           -> (Highlight, Set Point)
           -> (Highlight, Set Point)
findGroup' game@Game{boardSize=s} stone p@(Point x y) same
    | x < 1 || x > s || y < 1 || y > s = same
    | p `member` stonePoints = same
    | p `member` seenPoints = same
    | p `member` resultPoints = same
    | st == getOppositeStone stone = other
    | st /= stone = point
    | otherwise = getAdj' p
                & List.foldr (findGroup' game stone) ident
    where ((stonePoints, resultPoints), seenPoints) = same
          ident = ((p|:stonePoints, resultPoints), seenPoints)
          other = ((stonePoints, resultPoints), p|:seenPoints)
          point = ((stonePoints, p|:resultPoints), seenPoints)
          st = seekBoard game p

-- Groups ------------------------------------------------------------- Holes --

-- | 'getHoles' returns the liberties of groups inside "hole"(s) via 'spiralFold''
getHoles :: Game -> Map Point Stone
         -> Highlight
         -> Int
         -> (Set Point, [Set Point])
getHoles Game{meta=md,boardSize=s} m (grp, lbs) d
    = lbs
    & Set.foldl hole (Set.empty, Map.empty)
    & snd             --- ^^^^^ /    ^^^^^ --- "open"/"closed" points
    & to2
    & bimap keysSet
            (nub . Map.elems)
    where Meta{spiral=sp} = md
          hole r@(o, c) p -- if (not) a hole, ALL path liberties are ("open") "closed"
              | p `member` o = r          -- "open"
              | not $ p `notMember` c = r -- "closed"
              | otherwise = let (ps, open) = magic p s d
                                           & spiralFold' False id path False sp
                         in if open
                            then (ps |++ o, c)
                            else (o, Set.foldr (`Map.insert` ps) c (p|:ps))
              where path (pt@(Point x y), _, _)
                        | pt `member` o            = (Nothing,    True ) -- "open"
                        | pt `member` grp          = (Just False, False)
                        | x < minx || y < miny || x > maxx || y > maxy ||
                          seekBoard' m pt /= Empty = (Just False, True )
                        | otherwise                = (Just True,  False)
          minx = Set.map (fst . coord) lbs & List.minimum
          miny = Set.map (snd . coord) lbs & List.minimum
          maxx = Set.map (fst . coord) lbs & List.maximum
          maxy = Set.map (snd . coord) lbs & List.maximum

-- | 'getHoles'' is used together with 'findGroup''
-- ^ and returns the holes via 'getHoles' with points excluding
-- ^ those of stones "inside" them
getHoles' :: Game
          -> (Highlight, Set Point)
          -> [Set Point]
getHoles' game@Game{board=m} ((grp, lbs), lbs')
    = getHoles game' m' (grp, lbs |++ lbs') 0
    & snd
    & to2
    & second (Set.filter ((/= Empty) . (m !))
             . keysSet
             . Map.restrictKeys m
             . Set.unions)
    & to2
    & bimap fst
            (to1 replicate . first length)
    & to1 zip
    & fmap (to1 Set.difference)
    where game'@Game{board=m'} = game{board=Map.restrictKeys m grp}

--------------------------------------------------------------------------------

-- | 'killGame' changes game status from 'Alive' to 'Dead' and increases the score of 'Black'
-- as 'White' plays the last 'Pass'.
killGame :: Game -> Game
killGame game@(Game md mm (eko,_) m lm s sb sw Alive) =
    pipe (Game md' mm (eko,[]) m lm s (sb+1) sw Dead)
    where md' = md{redo=Nothing,followup=createNumbering}
          pipe = zombieless . withUndoGame game
killGame game = game

-- | 'removeKo' removes 'Ko' from the 'Game'
removeKo :: Game -> Game
removeKo game@(Game md mm (eko,_) m lm s b w status) = Game md mm (eko,[]) newBoard lm s b w status
    where newBoard = List.foldl removePiece m (getPointsKo game)

-- | 'addPiece' adds a element of type t to map
addPiece :: t -> Point -> (Map Point t) -> (Map Point t)
addPiece stone point m = Map.insert point stone (Map.delete point m)

-- | 'removePiece' removes the stone from given point in the map
removePiece :: (Map Point t) -> Point -> (Map Point t)
removePiece m point = Map.delete point m

-- | 'putPiece' adds a 'Stone' at 'Point's to the board of 'Game'
putPiece :: Stone -> [Point] -> Game -> Game
putPiece stone points game =
    game{board=foldr' (addPiece stone) points (board game)}

-- | 'playPass' adds 'Pass' move by the 'Stone' on the 'Game'
playPass :: Game -> Stone -> Game
playPass game@Game{maxMoves=m,lastMove=Move n _ _} _ | n < m = game
playPass game@(Game _ _ _ _ lm _ sb sw _) stone = pipe game {
  lastMove = Pass stone
, scoreBlack = newsb
, scoreWhite = newsw
} where
    -- | Increase score of opposite stone
    newsb = if stone == Black then sb else (sb+1)
    newsw = if stone == White then sw else (sw+1)
    pipe = voidPlay . removeKo . withUndoGame game
         . case lm of Skip _ -> toggleSkip
                      _      -> id

-- Play ------------------------------------------------------------------------

-- | 'nextTurn' occurs after 'voidPlay', 'playMove'' or 'maxMovesDecrement'
nextTurn :: Bool -> Game -> Game
nextTurn play game@Game{meta=md} = game { meta = md {
  uuid = Nothing
, redo = bool (redo md) Nothing play
, groupsSelected = ([],[])
, followup = createNumbering{enabled=efu,numeric=n}
}
} where Meta{followup=Numbering{enabled=efu,numeric=n}} = md

-- | 'voidPlay' occurs from 'toggleSkip' or 'playPass'
voidPlay :: Game -> Game
voidPlay game@Game{meta=Meta{sanityKo=sko}} = pipe game
    where pipe = nextTurn True . bool id playKo' sko . removeKo'

-- Play ---------------------------------------------------------------- Skip --

-- | 'toggleSkip' adds 'Skip' move on the 'Game' unless removes it
toggleSkip :: Game -> Game
toggleSkip game@Game{lastMove=Move _ (Point 0 0) _} = game
toggleSkip game@Game{lastMove=Pass _} = game
toggleSkip game@Game{lastMove=Skip game'} = withLookGame' game game'
toggleSkip game@Game{maxMoves=m,lastMove=Move n _ _} | n == m = game
toggleSkip game@Game{meta=md} = voidPlay game {
  meta = md { undo = Nothing }
, lastMove = Skip game
}

-- Play ------------------------------------------------------------ FollowUp --

-- | 'somePlay' occurs from 'playMove' or 'playSafe' and returns via 'playing'
-- ^ if there are followup points, there is no play at all (returns 'Nothing'),
-- ^ unless 'Point' has the highest priority
somePlay :: Game
         -> Int
         -> Point
         -> (Stone -> Game -> Int -> Game)
         -> IO (Maybe Game)
somePlay game n point play' = do
    fup <- best game
    case fup of
      Just (f, (ps@(p:_), r))
        | p == point -> do
                        gcTree game
                        let pipe' = if count + n < mm && r
                                    then rest fu ps else id
                        if f
                        then do
                          deck <- addDeck game
                          let game' = pipe deck
                          return $ Just (pipe' game')
                        else
                          let game' = pipe game in
                          return $ Just (pipe' game')
      Nothing        -> do
                        gcTree game
                        return $ Just (pipe game)
      _              -> return Nothing
    where Game{meta=Meta{followup=fu},maxMoves=mm,lastMove=lm} = game
          pipe = playKo' . playing play' . removeKo'
          count = mod (getMoveCount lm) mm

-- Play --------------------------------------------------------------- -Skip --

-- | 'playing' occurs from 'somePlay' or 'validating' and toggles any 'Skip'
playing :: (Stone -> Game -> Int -> Game)
        -> Game
        -> Game
playing play' game@Game{maxMoves=mm,lastMove=lm} = pipe game
    where stone = getTurnStoneFromMove lm mm
          count = getMoveCount lm
          pipe = withUndoGame game
               . to1 (play' stone)
               . to1 ($)
               . bimap' (to1 bimap . (,id) . flip ($))
                        case lm of Skip _ -> (toggleSkip, mm)
                                   _      -> (id, count)
               . to2

-- Play ---------------------------------------------------------------- Safe --

-- | 'playSafe' plays 'Point's via 'playSafe''
-- It is without validation and not sound for Ko
-- There is no undo (except last, or undo last move removes all)
playSafe :: [Point] -> Game -> IO (Maybe Game)
playSafe ps@(pt:_) game@Game{meta=md} = do
    game' <- to1 ($)
           . bimap (somePlay game (length ps))
                   playSafe'
           $ (pt, ps)
    return $ fmap pipe game'
    where Meta{deadZombies=zs,liveZombies=sz} = md
          pipe = bool mirrorZombies id (isNothing zs && isNothing sz)

-- | 'playSafe'' plays 'head' point via 'playMove''
-- ^ after adding the 'tail' via 'putPiece'
playSafe' :: [Point] -> Stone -> Game -> Int -> Game
playSafe' (p:ps) stone game count = putPiece stone ps game{meta=md'}
                                  & deadSafe stone ps
                                  & flip (playMove' p stone) count'
    where nr' = foldl' incNumbering ps nr
          md' = md{numbering=nr'}
          count' = mod (count + length ps) mm
          Meta{numbering=nr} = md
          Game{meta=md,maxMoves=mm} = game

-- | 'deadSafe' occurs from 'playSafe' and removes trapped groups
-- ^ via 'removeGroups'
deadSafe :: Stone -> [Point] -> Game -> Game
deadSafe stone = curry
               $ snd . to1 (`removeGroups` ostone)
               & curry
               & foldr'
               & to1
    where ostone = getOppositeStone stone

-- | 'getSafePoints' returns the points added to 'Game' by 'playSafe'
getSafePoints :: Game -> [Point]
getSafePoints Game{lastMove=Skip _} = []
getSafePoints game@Game{meta=Meta{undo=Just game'}} =
    case lm of
      Move _ pt st -> (m, m')
                    & bimap1 (Map.filter (== st))
                    & to1 Map.difference
                    & keys
                    & List.delete pt
      _            -> []
    where Game{board=m,lastMove=lm} = game
          Game{board=m'} = case lastMove game' of
                             Pass _ -> game'
                                     & undo . meta
                                     & purify
                             _      -> game'
getSafePoints _ = []

-- Play ---------------------------------------------------------------- Move --

-- | 'playMove' adds 'Move' to 'Point' on the 'Game'.
-- After playing the move, also removes the captured points of the opposite stone if present
playMove :: Game -> Point -> IO (Maybe Game)
playMove game@Game{meta=md} point = do
    game' <- to1 ($)
           . bimap (somePlay game 1)
                   playMove'
           . to2
           $ point
    return $ fmap pipe game'
    where Meta{deadZombies=zs,liveZombies=sz} = md
          pipe = bool mirrorZombies id (isNothing zs && isNothing sz)

-- | 'playMove'' adds the 'Int'th move of 'Stone' at 'Point' on the 'Game'
-- ^ it does actually play move
playMove' :: Point -> Stone -> Game -> Int -> Game
playMove' point stone game count = pipe game { meta = md {
  numbering = incNumbering nr point
, highlight = (ehl,Empty,[])
}
, lastMove = Move (mod count mm + 1) point stone
} where Game{meta=md,maxMoves=mm} = game
        Meta{numbering=nr,highlight=(ehl,_,_)} = md
        ostone = getOppositeStone stone
        -- | 'updateBoard' removes Ko points prior to adding stone to board
        updateBoard = putPiece stone [point]
                    . if mod count mm == 0 || validKo game (Just stone) point
                      then removeKo else id
        -- | 'updateBoardKo' removes Ko points post capture unless a Ko is formed
        updateBoardKo (Just p, game') = addKo game' p
        updateBoardKo (_, game') = removeKo game'
        pipe = nextTurn True
             . updateBoardKo
             . removeGroups point ostone
             . updateBoard

--------------------------------------------------------------------------------

-- | 'removeGroups' removes captured stones of 'Stone' from the 'Game' starting from
-- 'Point' and moving in all four directions
removeGroups :: Point -> Stone -> Game -> (Maybe Point, Game)
removeGroups point stone game@(Game md@Meta{numbering=nr} _ _ m _ _ _ _ _) =
    (if length pointsToBeRemoved == 1 && isKo then Just (pointsToBeRemoved !! 0) else Nothing, game')
    where (up, down, left, right) = getAdj point
          removeUp = removeDead up stone game
          removeDown = removeDead down stone game
          removeLeft = removeDead left stone game
          removeRight = removeDead right stone game
          pointsToBeRemoved = removeUp ++ removeDown ++ removeLeft ++ removeRight
          point' = if length removeUp == 1 then up else if length removeDown == 1 then down else if length removeLeft == 1 then left else right
          (up', down', left', right') = getAdj point'
          stone' = getOppositeStone stone
          isKo = length ((removeDead up' stone' game) ++ (removeDead down' stone' game) ++ (removeDead left' stone' game) ++ (removeDead right' stone' game)) == 1
          ll = foldl' removePiece pointsToBeRemoved (labelling nr)
          m' = foldl' removePiece pointsToBeRemoved m
          md' = md{numbering=nr{labelling=ll}}
          game' = updateScore game{meta=md',board=m'} (length pointsToBeRemoved) stone'

-- | 'getAdj' returns adjacent points of a given 'Point'
getAdj :: Point -> (Point, Point, Point, Point)
getAdj (Point x y) = (Point x (y-1), Point x (y+1), Point (x-1) y, Point (x+1) y)

-- | 'getAdj'' returns ordered list of adjacent points of a given 'Point'
getAdj' :: Point -> [Point]
getAdj' point = let (up, down, left, right) = getAdj point
                in [up, down, left, right]

-- | 'removeDead' returns dead groups of 'Stone' in 'Game' starting from 'Point'
removeDead :: Point -> Stone -> Game -> [Point]
removeDead point stone game = case removablePoints of
                                Nothing:_ -> []
                                _         -> purifyList removablePoints
                              where removablePoints = (findTrappedGroup game point stone [])

-- | 'updateScore' updates the score of 'Stone' by given 'Int'
updateScore :: Game -> Int -> Stone -> Game
updateScore game@(Game md mm kos m lm s b w status) p st
    | st == Black = (Game md mm kos m lm s (b+p) w status)
    | otherwise = (Game md mm kos m lm s b (w+p) status)

-- | 'addKo' adds 'Ko' to the game at given point
addKo :: Game -> Point -> Game
addKo game@Game{pointsKo=(False,kos@(_:_)),board=m} p =
    putPiece Ko [p] game{board=removePiece m (head kos),pointsKo=(False,p:kos)}
addKo game@Game{pointsKo=(eko,kos)} p = putPiece Ko [p] game{pointsKo=(eko,p:kos)}

-- | 'addKo'' adds 'Ko' prime to the game at given points
addKo' :: Game -> [Point] -> Game
addKo' game@Game{meta=md} kos' =
    let Game _ mm kos m lm s b w status = game
    in Game md' mm kos (List.foldr (addPiece Ko) m kos') lm s b w status
    where md' = md{pointsKo'=kos'}

-- | 'seekBoard' returns the 'Stone' at given 'Point' in the 'Game'
seekBoard :: Game -> Point -> Stone
seekBoard (Game _ _ _ m _ _ _ _ _) p = case Map.lookup p m of
    Just stone -> stone
    Nothing -> Empty

-- | 'seekBoard'' returns the 'Stone' at given 'Point' - Empty for Ko
seekBoard' :: Map Point Stone -> Point -> Stone
seekBoard' m p = case m !? p of
    Just Ko -> Empty
    Just stone -> stone
    Nothing -> Empty

-- | This data type represents whether corresponding stone belongs to Black or White or None
data Status = Seen | Unseen | SeenW | SeenB | None deriving (Eq)

-- | 'seekMap' returns the 'Status' of 'Point' from a given map
seekMap :: (Map Point Status) -> Point -> Status
seekMap m p = case Map.lookup p m of
    Just s -> s
    Nothing ->  Unseen

-- | 'findTrappedGroup' finds trapped group of stones in the game
-- starting from point and going in all directions
findTrappedGroup :: Game -> Point -> Stone -> [Maybe Point] -> [Maybe Point]
findTrappedGroup _ _ _ (Nothing:_) = [Nothing]
findTrappedGroup game@(Game _ _ _ _ _ boardSize _ _ _) point@(Point x y) stone seenPoints
    | x < 1 || x > boardSize || y < 1 || y > boardSize = seenPoints
    | elem (pure point) seenPoints = seenPoints
    | seekBoard game point == Empty = Nothing:seenPoints
    | seekBoard game point == Ko = Nothing:seenPoints
    | seekBoard game point /= stone = seenPoints
    | otherwise = findTrappedGroup game left stone
        $ findTrappedGroup game right stone
        $ findTrappedGroup game up stone
        $ findTrappedGroup game down stone ((pure point):seenPoints)
    where (up, down, left, right) = getAdj point

-- | 'findTerritory' finds territory of a given point in the game
-- starting from point and going in all directions
findTerritory :: Game -> Point -> Stone -> ((Map Point Status), [Maybe Point]) -> ((Map Point Status), [Maybe Point])
findTerritory game@(Game _ _ _ _ _ boardSize _ _ _) point@(Point x y) stone (m, points)
    | x < 1 || x > boardSize || y < 1 || y > boardSize = (m, points) -- If point is out of board
    | elem (pure point) points = (m, points) -- If we have already visited this point
    | seekBoard game point == getOppositeStone stone = (m, Nothing:points) -- If this point has opposite stone
    | seekBoard game point == stone = (m, points) -- If this point has current stone
    | otherwise = findTerritory game left stone
        $ findTerritory game right stone
        $ findTerritory game up stone
        $ findTerritory game down stone ((Map.insert point Seen m), ((pure point):points)) -- Otherwise recure on all adjacent points of the stone
    where (up, down, left, right) = getAdj point

-- | 'findTerritories' finds terrirtories of either Black or White stone from given Point
findTerritories :: Game -> Point -> (Map Point Status) -> (Map Point Status)
findTerritories game point m
    | seekBoard game point /= Empty && seekBoard game point /= Ko = addPiece None point m -- If point has Ko or Empty
    | seekMap m point == SeenW = m -- If point belongs to White's territory
    | seekMap m point == SeenB = m -- If point belongs to Black's territory
    | seekMap m point == None = m -- If point has been seen and belongs to none
    | otherwise = if elem Nothing (snd tw) then
                      if elem Nothing (snd tb) then setInMap m (snd tb) None
                      else setInMap m (snd tb) SeenB
                  else setInMap m (snd tw) SeenW
    where tb = findTerritory game point Black (m, [])
          tw = findTerritory game point White (m, [])

-- | 'setInMap' sets staus of points in the map
setInMap :: (Map Point Status) -> [Maybe Point] -> Status -> (Map Point Status)
setInMap m [] st = m
setInMap m (point:points) st | point /= Nothing = setInMap (addPiece st (purify point) m) points st
                             | otherwise = setInMap m points st

-- | 'findAllTerritoriesOfPoints' calulates all the territories of given list of points in Game
findAllTerritoriesOfPoints :: Game -> [Point] -> (Map Point Status) -> (Map Point Status)
findAllTerritoriesOfPoints game [] m = m
findAllTerritoriesOfPoints game (point:points) m = findAllTerritoriesOfPoints game points (findTerritories game point m)

-- | 'findTerritories' creates list of points and finds territories for all the points in the game
findAllTerritories :: Game -> (Map Point Status)
findAllTerritories game@(Game _ _ _ _ _ boardSize _ _ _) = findAllTerritoriesOfPoints game points Map.empty
    where points = [(Point x y) | x <- [1..boardSize], y <- [1..boardSize]]

-- | 'finishGame' finds all the captured territories in the 'Game' and updates the correspoing scores of the 'Stone'
finishGame :: Game -> Game
finishGame game@(Game md mm _ m lm s sb sw status) =
    withUndoGame game (Game md' mm (False,[]) m lm s sb' sw' Over)
    where sb' = sb + countSeenB t s
          sw' = sw + countSeenW t s
          t = findAllTerritories game
          md' = (createMeta' s){numbering=nr
                               ,highlight=(ehl,Empty,[])
                               ,sanityKo=sko
                               ,tree=tr
                               ,showHelp=h}
          Meta _ _ nr _ (ehl,_,_) _ _ _ _ sko _ _ _ _ tr _ h = md

-- | 'countSeenB' counts the points that belong to territory of Black
countSeenB :: (Map Point Status) -> Int -> Int
countSeenB m size | l == size*size = 0 -- If board is empty then it cannot be territory
                  | otherwise = l
                  where l = length $ List.filter (\(p,s) -> s == SeenB) (assocs m)

-- | 'countSeenW' counts the points that belong to territory of White
countSeenW :: (Map Point Status) -> Int -> Int
countSeenW m size | l == size*size = 0 -- If board is empty then it cannot be territory
                  | otherwise = l
                  where l = length $ List.filter (\(p,s) -> s == SeenW) (assocs m)

-- | 'purify' changes Maybe Type to Type
purify :: Maybe a -> a
purify (Just a) = a

-- | 'purifyList' changes Maybe Type to Type for a list
purifyList :: [Maybe a] -> [a]
purifyList = fmap purify . List.filter isJust

-- | 'validKo' checks if 'Stone' can play at Ko 'Point' (or if Ko color is the same)
validKo :: Game -> Maybe Stone -> Point -> Bool
validKo game@Game{meta=md,maxMoves=m,lastMove=Move n _ st} st' pt =
    n < m && (fromMaybe st st' == st) && pt `elem` getPointsKo game && notElem pt kos'
    where Meta{pointsKo'=kos'}=md
validKo _ _ _ = False

-- | 'validMove' checks if the move of given 'Point' is valid or not
validMove :: Game -> Point -> Bool
validMove game@(Game _ mm _ m lm s _ _ _) p@(Point x y)
    | x < 1 || x > s || y < 1 || y > s = False
    | validKo game Nothing p = True
    | seekBoard game p /= Empty = False -- If point is not empty
    | not $ checkIfTrapped game1 p st = True -- If point is not trapped then validmove
    -- If point is trapped then check if on placing the stone whether opposites stones are captured or not
    | (seekBoard game up == ostone) && (checkIfTrapped game1 up ostone) = True
    | (seekBoard game down == ostone) && (checkIfTrapped game1 down ostone) = True
    | (seekBoard game left == ostone) && (checkIfTrapped game1 left ostone) = True
    | (seekBoard game right == ostone) && (checkIfTrapped game1 right ostone) = True
    | otherwise = False
    where game1 = putPiece st [p] game
          st = getTurnStoneFromMove lm mm
          ostone = getOppositeStone st
          (up, down, left, right) = getAdj p

-- | 'checkIfTrapped' checks whether a stone is trapped or not
checkIfTrapped :: Game -> Point -> Stone -> Bool
checkIfTrapped game p st = not $ elem Nothing (findTrappedGroup game p st [])

-- | 'checkIfNothing' checks whether 'Maybe Point' is 'Nothing'
checkIfNothing :: Maybe t -> Bool
checkIfNothing Nothing = True
checkIfNothing (Just _) = False

-- | 'getWinner' declares the winner depending on the scores of the stones
getWinner :: Game -> String
getWinner game@(Game _ _ _ _ _ _ sb sw _)
    | sb > sw = "Black wins." -- If 'Black' score is more than 'White' score
    | sw > sb = "White wins." -- If 'White' score is more than 'Black' score
    | otherwise = "Game Draws" -- If both the scores are equal
