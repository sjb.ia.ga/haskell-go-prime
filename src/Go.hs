{-|
    This module has only one function 'playGo' that asks
    user for size as input, creates a game and starts the game window.
-}
module Go(playGo) where

import Data.Bifunctor as Bifunctor
import Data.Function as Function
import Data.Bool as Bool
import Data.Maybe as Maybe
import Data.Char as Char
import Data.List as List
import Data.Map as Map hiding ((\\), drop, filter, foldl, foldr, map, null, take)
import BoardGo
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game

-- | 'PlayGo' functions asks for size of board, creates a 'Game' object and starts the game display
playGo :: IO ()
playGo = do
    putStrLn "Please enter the size of the board - 9 13 or 19"
    sizeString <- getLine
    let size = read sizeString
    -- If invalid size is given then ask again
    if size /= 9 && size /= 13 && size /= 19 then playGo else do
        game <- createGame size
        playGame size game

playGame :: Int -> Game -> IO ()
playGame size game' = do
  let window = InWindow "Go Window" (40 * (size+2) + 360, 40 * (size+2) +480) (20, 20)
  playIO window (dark yellow) 0 game' render handleEvent (\_ game -> return game)

-- | 'zombiesPoint' checks if 'Point' is along zombies
zombiesPoint :: Int -> BoardGo.Point -> Bool
zombiesPoint s (Point x y) = (x == 0 || x == s+1) && (0 <= y && y <= s+1)
                          || (y == 0 || y == s+1) && (0 <= x && x <= s+1)

-- | 'toPoint' returns the board 'Point' for 'Event'
toPoint :: Event -> Int -> Maybe BoardGo.Point
toPoint (EventKey _ _ _ (x, y)) s = Just p
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
toPoint (EventMotion (x, y)) s = Just p
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
toPoint _ _ = Nothing

-- | 'handleEvent' handles mouse click, motion and keyboard events
handleEvent :: Event -> Game -> IO Game

-- PENDING ----------------------------------------------------------- LETTER --

-- ^ If left/right/middle-clicked on point on alive game while a letter label is pending, then
-- ^ label with pending letter (max moves must be 1 and numbering be numeric)
handleEvent (EventKey (MouseButton _) Down _ (x, y)) game@(Game (Meta _ _ nr (Just [_]) _ _ _ _ _ _ _ _ _ _ _ _ _) 1 _ m _ s _ _ Alive)
    | n && st /= Empty = return $ usePendingLetter p game
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          st = seekBoard' m p
          Numbering{numeric=n} = nr

-- PENDING ------------------------------------------------------------ HINT ---

-- ^ If a command key is pressed on keyboard on any game, then set pending text/hint
handleEvent (EventKey (Char c) Down _ _) game@(Game md _ _ _ _ _ _ _ status)
    | c `elem` o && (decksBoard |-> c == 'd') =
        let t = case c of
                  'p' -> "pass"
                  '+' -> "increment"
                  '-' -> "decrement"
                  '*' -> "double"
                  '/' -> "half"
                  '0' -> "reset"
                  '1' -> "numeric"
                  'a' -> "alphabetic"
                  'h' -> "help"
                  'l' -> "labels"
                  'u' -> "undo"
                  'r' -> "redo"
                  'g' -> "highlight"
                  '"' -> "multi-Ko"
                  '\''-> "Ko'"
                  's' -> "sweep"
                  'z' -> "zombies"
                  'd' -> "decks"
                  _   -> "end"
        in return $ pendingText t game
    where o | status == Alive = "p+-*/01ahlurg\"'szd"
            | status == Dead = "hurge"
            | otherwise = "hug"
          decksBoard = maybe False ((== "dc") . take 2) pl
          Meta _ _ _ pl _ _ _ _ _ _ _ _ _ _ _ _ _ = md

-- PENDING ------------------------------------------------------------ CLEAR --

-- ^ On any event other than "a command key pressed", clear pending hint
-- ^ while possibly restoring [previous] pending op-code
handleEvent event game@(Game (Meta _ _ _ (Just t) _ _ _ _ _ _ _ _ _ _ _ _ _) _ _ m _ s _ _ _)
    | length t >= 3 && t !! 2 /= ' ' = handleEvent event (pipe game')
    where pt = toPoint event s
          p = purify pt
          st = seekBoard' m p
          pipe = bool id pipe' (maybe False ((`elem` ["hl", "gs"]) . take 2) pl)
          pipe' = bool (clearPending True) pipe'' (ehl && isJust pt && boardPoint s p)
          pipe'' = p & bool reHighlightAt rePointHighlightAt (st == Empty)
          game' = clearPending (isJust (elemIndex ' ' t)) game
          Game (Meta _ _ _ pl (ehl,_,_) _ _ _ _ _ _ _ _ _ _ _ _) _ _ _ _ _ _ _ _ = game'

-- LEFT/RIGHT/MIDDLE CLICK | MOTION | KEY ----------------------- DECKS BOARD --

-- ^ If left/right/middle-clicked on alive game at point on the decks board, then
-- ^ goto deck if found, or otherwise guard
handleEvent (EventKey (MouseButton _) Down _ (x, y)) game@(Game md _ _ _ _ s _ _ Alive)
    | decksBoard = gotoDeck pt game . pipe $ game
    where pt = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          decksBoard = maybe False ((== "dc") . take 2) pl
          pipe = clearPending True
          Meta _ _ _ pl _ _ _  _ _ _ _ _ _ _ _ _ _ = md

-- ^ If motion on alive game on the decks board, then guard
handleEvent (EventMotion _) game@(Game md _ _ _ _ _ _ _ Alive)
    | decksBoard = return game
    where decksBoard = maybe False ((== "dc") . take 2) pl
          Meta _ _ _ pl _ _ _ _ _ _ _ _ _ _ _ _ _ = md

-- ^ If any key except d is released on keyboard on alive game on the decks board,
-- ^ then guard
handleEvent (EventKey (Char k) Up _ (x, y)) game@(Game md _ _ m _ s _ _ Alive)
    | decksBoard && k /= 'd' = return game
    | decksBoard && k == 'd' = return $ clearPending True game & pipe pt
    where pt = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          st = seekBoard' m pt
          pipe = bool reHighlightAt rePointHighlightAt (st == Empty)
          decksBoard = maybe False ((== "dc") . take 2) pl
          Meta _ _ _ pl _ _ _ _ _ _ _ _ _ _ _ _ _ = md

-- LEFT CLICK ----------------------------------------------------- PLAY MOVE --

-- ^ If clicked on point on alive game, then check if valid move and play the move,
-- ^ possibly using a pending letter, while possibly re-highlighting
handleEvent (EventKey (MouseButton LeftButton) Down _ (x, y)) game@(Game md _ _ _ lm s _ _ Alive)
    | validMove game p = do
                         game' <- playMove game p
                         return $ maybe game pipe game'
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          pipe = reHighlightAt p . clearPending False . maybe id (const (usePendingLetter p)) pl
          Meta _ _ _ pl _ _ _ _ _ _ _ _ _ _ _ _ _ = md
-- ^ If clicked on point on alive game, clear a possible pending letter, except any other pending op-code
handleEvent (EventKey (MouseButton LeftButton) Down _ (x, y)) game@(Game (Meta _ _ _ (Just [_]) _ _ _ _ _ _ _ _ _ _ _ _ _) _ _ _ _ s _ _ Alive)
    | boardPoint s p = return $ clearPending False game
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))

-- LEFT CLICK ------------------------------------------------ TOGGLE ZOMBIES --

-- ^ If clicked on born zombie on alive game, then "toggle" live or uncounted dead zombie
-- ^ If clicked on uncounted dead zombie on alive game, then "toggle" dead zombie
-- ^ If clicked on live/dead zombie on alive game, then toggle live zombie
-- ^ If clicked on no zombie on alive game, then add live zombie
-- ^ LAST EQUATION FOR LEFT-CLICK (Alive)
handleEvent (EventKey (MouseButton LeftButton) Down _ (x, y)) game@(Game _ _ _ _ _ s _ _ Alive)
    | p `member` bz = return $ game' & pipe (bool toggleDeadZombies toggleLiveZombies (st == Empty))
    | st' /= Empty = return $ game' & if st /= st' && st /= Empty
                                      then pipe toggleDeadZombies . toggleLiveZombies False p -- "remove" live
                                      else pipe (bool toggleLiveZombies toggleDeadZombies (st == st'))
    | st == Empty && zombiesPoint s p = return $ game' & pipe toggleLiveZombies
    | st /= Empty = return $ game' & pipe toggleDeadZombies
    | otherwise = return game
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          st = maybe Empty (`seekBoard'` p) zs
          st' = maybe Empty (`seekBoard'` p) sz
          md' = Meta is sp nr pl (ehl,Empty,[]) gs u r kos' sko zs bz sz fu tr ps h
          game' = Game md' mm kos m lm s b w Alive
          pipe t = mergeBornZombies
                 . t False p
                 . bool id (clearPending True) (getOppositeStone hlst /= Empty)
          Game md mm kos m lm _ b w _ = game
          Meta is sp nr pl (ehl,hlst,_) gs u r kos' sko zs bz sz fu tr ps h = md

-- RIGHT CLICK --------------------------------------------------- MULTI-UNDO --

-- ^ If right-clicked on alive game at stone, then undo game to point, while re-highlighting
handleEvent (EventKey (MouseButton RightButton) Down _ (x, y)) game@(Game _ _ _ m _ s _ _ Alive)
    | seekBoard' m pt /= Empty = do
                                 game' <- undoGameTo pt game
                                 return $ pipe game'
    where pt = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          pipe = reHighlightAt pt

-- RIGHT/MIDDLE CLICK ---------------------------------------------- FOLLOWUP --

-- ^ If right/middle-clicked on alive game at point, then add/remove/swap followup label
handleEvent (EventKey (MouseButton mb) Down _ (x, y)) game@(Game md _ _ _ lm s _ _ Alive)
    | e && case lm of Move{} -> True; _ -> False
        && isJust (fmap (const Nothing)
                        (validating game pt)) = pipe game
    where pt = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          pipe = followupAt (mb == RightButton) pt
          Meta _ _ _ _ _ _ _ _ _ _ _ _ _ Numbering{enabled=e} _ _ _ = md

-- RIGHT CLICK ----------------------------------------------- REMOVE ZOMBIES --

-- ^ If right-clicked on live zombie on alive game, then remove it
-- ^ If right-clicked on dead zombie on alive game, count it as removed
handleEvent (EventKey (MouseButton RightButton) Down _ (x, y)) game@(Game _ _ _ _ _ s _ _ Alive)
    | st' /= Empty && st == st' = return game -- uncounted dead
    | st' /= Empty && st /= Empty = return $ game' & pipe toggleDeadZombies . toggleLiveZombies True p -- "remove" both
    | st' /= Empty = return $ game' & pipe toggleLiveZombies
    | st /= Empty = return $ game' & pipe toggleDeadZombies
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          st = maybe Empty (`seekBoard'` p) zs
          st' = maybe Empty (`seekBoard'` p) sz
          md' = Meta is sp nr pl (ehl,Empty,[]) gs u r kos' sko zs bz sz fu tr ps h
          game' = Game md' mm kos m lm s b w Alive
          pipe t = mergeBornZombies
                 . t True p
                 . bool id (clearPending True) (getOppositeStone hlst /= Empty)
          Game md mm kos m lm _ b w _ = game
          Meta is sp nr pl (ehl,hlst,_) gs u r kos' sko zs bz sz fu tr ps h = md

-- MIDDLE CLICK ------------------------------------- TOGGLE SPANNING ZOMBIES --

-- ^ If middle-clicked on dead/live zombie on alive game, then toggle spanning live/dead zombies
handleEvent (EventKey (MouseButton MiddleButton) Down _ (x, y)) game@(Game _ _ _ _ _ s _ _ Alive)
    | p `member` bz = return $ toggleZombiesSpan p (bz ! p) game' & pipe
    | st' /= Empty && st == st' = return game -- uncounted dead
    | st' /= Empty = return $ toggleZombiesSpan p st' game' & pipe
    | st /= Empty = return $ toggleZombiesSpan p st game' & pipe
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          st = maybe Empty (`seekBoard'` p) zs
          st' = maybe Empty (`seekBoard'` p) sz
          md' = Meta is sp nr pl (ehl,Empty,[]) gs u r kos' sko zs bz sz fu tr ps h
          game' = Game md' mm kos m lm s b w Alive
          pipe = mergeBornZombies . bool id (clearPending True) (getOppositeStone hlst /= Empty)
          Game md mm kos m lm _ b w _ = game
          Meta is sp nr pl (ehl,hlst,_) gs u r kos' sko zs bz sz fu tr ps h = md

-- MIDDLE CLICK ------------------------------------------------ MULTI SELECT --

-- ^ If middle-clicked on opposite stone on alive game, then
-- ^ toggle group from selection
handleEvent (EventKey (MouseButton MiddleButton) Down _ (x, y)) game@(Game _ mm _ m lm s _ _ Alive)
    | st == st' = return $ toggleFromGS pt game & pipe
    | st == getOppositeStone st' = return game
    where pt = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          st = seekBoard' m pt & getOppositeStone
          st' = getTurnStoneFromMove lm mm
          pipe = reHighlightAt pt

-- RIGHT/MIDDLE CLICK ------------------------------------ MULTI LIVE-CAPTURE --

-- ^ If right/middle-clicked on point on alive game, and no pending selected groups, then
-- ^ check if valid live group capture move and [safe] play the move, while re-highlighting
handleEvent (EventKey (MouseButton _) Down _ (x, y)) game@(Game (Meta _ _ _ pl _ (_,[]) _ _ kos' _ _ _ _ fu _ _ _) _ _ _ _ s _ _ Alive)
    | not e &&
      not (null lbs) = do
                       let kos = getPointsKo game ++ kos'
                       if null $ lbs `intersect` kos
                       then do
                         game' <- game & playSafe lbs
                         return $ maybe game pipe game'
                       else
                         return game
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          moves = getMaxMovesFromLastMove game
          lbs = liveGroupCapture game p moves
          pipe = maybe id (const (usePendingLetter p)) pl . reHighlightAt p
          Numbering{enabled=e} = fu

-- RIGHT/MIDDLE CLICK ---------------------------------- MULTI-SELECT CAPTURE --

-- ^ If right/middle-clicked on point on alive game, with pending selected groups, then
-- ^ check if valid multi-select group capture and [safe] play the move, while re-highlighting
handleEvent (EventKey (MouseButton _) Down _ (x, y)) game@(Game (Meta _ _ _ _ _ (_,_:_) _ _ kos' _ _ _ _ fu _ _ _) _ _ _ _ s _ _ Alive)
    | not e &&
      clamp' 1 moves lbs = do
                           let kos = getPointsKo game ++ kos'
                           if null $ lbs `intersect` kos
                           then do
                             game' <- playSafe lbs game
                             return $ maybe game pipe game'
                           else
                             return game
    where pt = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          moves = getMaxMovesFromLastMove game
          lbs = libertiesOfGSAt (Just pt) game & snd
          pipe = reHighlightAt pt
          Numbering{enabled=e} = fu

-- RIGHT/MIDDLE CLICK --------------------------------------------- SKIP TURN --

-- ^ If right/middle-clicked on [not board] point on alive game, with no valid move,
-- ^ with no valid live group capture, with no valid multi-select group capture,
-- ^ and no pending selected groups, then toggle skip[, while point re-highlighting]
-- ^ LAST EQUATION FOR RIGHT/MIDDLE-CLICK
handleEvent (EventKey (MouseButton _) Down _ (x, y)) game@(Game (Meta _ _ _ _ _ (_,[]) _ _ _ _ _ _ _ _ _ _ _) _ _ _ _ s _ _ Alive)
    | not (boardPoint s p) = return $ toggleSkip game
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))

-- KEY ----------------------------------------------------------------- PASS --

-- ^ If p is released on keyboard on alive game then play pass by current stone
handleEvent (EventKey (Char 'p') Up _ _) game@(Game _ _ _ _ (Pass Black) _ _ _ Alive) =
    return $ killGame game
handleEvent (EventKey (Char 'p') Up _ _) game@(Game md mm _ _ lm _ _ _ Alive) =
    do
    kos <- fuPointsKo game
    return $ bool game (playPass game stone) (tc == length kos)
    where stone = getTurnStoneFromMove lm mm
          Meta _ _ _ _ _ _ _ _ _ _ _ _ _ (Numbering _ _ _ tc) _ _ _ = md

-- KEY ------------------------------------------------------------ UNDO/REDO --

-- ^ If u is released on keyboard on any game, then undo, while re-highlighting
handleEvent (EventKey (Char 'u') Up _ (x, y)) game@(Game _ _ _ _ _ s _ _ _)
    | isUndoSane game = do
                        game' <- undoGame game
                        let st = seekBoard game' p & getOppositeStone
                        let pipe = bool reHighlightAt rePointHighlightAt (st == Empty)
                        return $ game' & bool id (pipe p) (boardPoint s p)
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
-- ^ If r is released on keyboard on any game, then redo, while re-highlighting
handleEvent (EventKey (Char 'r') Up _ (x, y)) game@(Game _ _ _ _ _ s _ _ _)
    | isRedoSane game = return $ game' & bool id (pipe p) (boardPoint s p)
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          st = seekBoard game' p & getOppositeStone
          game' = redoGame game
          pipe = bool reHighlightAt rePointHighlightAt (st == Empty)

-- KEY ------------------------------------------------------- PENDING LETTER --

-- ^ If A-Z is released on keyboard on alive game then flag pending letter
-- ^ (max moves be 1 and labels numeric)
handleEvent (EventKey (Char c') Up _ _) game@(Game _ 1 _ _ _ _ _ _ Alive)
    | n && 'A' <= c' && c' <= 'Z' = return $ pendingLetter game' c'
    where md' = Meta is sp nr Nothing (ehl,Empty,[]) gs u r kos' sko zs bz sz fu tr ps h
          game' = Game md' 1 kos m lm s sb sw Alive
          Game md _ kos m lm s sb sw _ = game
          Meta is sp nr _ (ehl,_,_) gs u r kos' sko zs bz sz fu tr ps h = md
          Numbering{numeric=n} = nr

-- KEY ------------------------------------------------------------ MAX MOVES --

-- ^ If + is released on keyboard on alive game then increment maximum moves
handleEvent (EventKey (Char '+') Up _ _) game@(Game _ _ _ _ _ _ _ _ Alive) =
    return $ maxMovesIncrement game
-- ^ If - is released on keyboard on alive game then decrement maximum moves
handleEvent (EventKey (Char '-') Up _ _) game@(Game _ _ _ _ _ _ _ _ Alive) =
    return $ maxMovesDecrement game
-- ^ If * is released on keyboard on alive game then double maximum moves
handleEvent (EventKey (Char '*') Up _ _) game@(Game (Meta _ _ _ (Just [_]) _ _ _ _ _ _ _ _ _ _ _ _ _) 1 _ _ _ _ _ _ Alive) =
    return game
handleEvent (EventKey (Char '*') Up _ _) game@(Game _ mm _ _ _ s _ _ Alive) =
    return $ take (min (2*mm) (s*s) - mm) [1..]
           & List.foldr (const maxMovesIncrement) game
-- ^ If / is released on keyboard on alive game then half maximum moves
handleEvent (EventKey (Char '/') Up _ _) game@(Game _ mm _ _ _ _ _ _ Alive) =
    return $ take (mm - div mm 2) [1..]
           & List.foldr (const maxMovesDecrement) game

-- KEY ----------------------------------------------------- NUMBERING/LABELS --

-- ^ If 0 is released on keyboard on alive game then reset numbering (either numeric or not)
handleEvent (EventKey (Char '0') Up _ (x, y)) game@(Game _ _ _ m _ s _ _ Alive) =
    resetNumbering (seekBoard' m p == Empty) game
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
-- ^ If 1 is released on keyboard on alive game with non-numeric labels then set labels to numeric
handleEvent (EventKey (Char '1') Up _ (x, y)) game@(Game md _ _ m _ s _ _ Alive)
    | not $ bool n u (seekBoard' m p == Empty) = game'
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          game' = toggleNumbering (seekBoard' m p == Empty) game
          Meta _ _ Numbering{numeric=n} _ _ _ _ _ _ _ _ _ _ Numbering{numeric=u} _ _ _ = md
-- ^ If a is released on keyboard on alive game with numeric labels then set labels to letters
handleEvent (EventKey (Char 'a') Up _ (x, y)) game@(Game md _ _ m _ s _ _ Alive)
    | bool n u (seekBoard' m p == Empty) = game'
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          game' = toggleNumbering (seekBoard' m p == Empty) game
          Meta _ _ Numbering{numeric=n} _ _ _ _ _ _ _ _ _ _ Numbering{numeric=u} _ _ _ = md

-- KEY --------------------------------------------------------------- TOGGLE --

-- ^ If h is released on keyboard on any game, then toggle help
handleEvent (EventKey (Char 'h') Up _ _) game = return game'
    where md' = Meta is sp nr Nothing hl gs u r kos' sko zs bz sz fu tr ps (not h)
          game' = Game md' mm kos m lm s b w status
          Game md mm kos m lm s b w status = game
          Meta is sp nr _ hl gs u r kos' sko zs bz sz fu tr ps h = md
-- ^ If l is released on keyboard on any game, then toggle numbering
handleEvent (EventKey (Char 'l') Up _ (x, y)) game@(Game _ _ _ m _ s _ _ status)
    | st == Empty |-> status == Alive = return $ game & toggleLabels (st == Empty)
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          st = seekBoard' m p
-- ^ If g is released on keyboard on any game, then start highlighting if disabled
handleEvent (EventKey (Char 'g') Up _ (x, y)) game@(Game (Meta _ _ _ _ (False,_,_) _ _ _ _ _ _ _ _ _ _ _ _) _ _ m _ s _ _ _)
    | st /= Empty = return $ game & pipe
    | otherwise = return $ game & pipe'
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          st = seekBoard' m p
          pipe = reHighlightAt p . toggleHighlight
          pipe' = rePointHighlightAt p . toggleHighlight
-- ^ If g is released on keyboard on any game, then just toggle highlight
handleEvent (EventKey (Char 'g') Up _ _) game = return $ toggleHighlight . clearPending True $ game
-- ^ If " is released on keyboard on alive game, then toggle enabling multiple Ko points
handleEvent (EventKey (Char '"') Up _ _) game@(Game _ _ _ _ _ _ _ _ Alive) = return $ toggleKos game
-- ^ If ' is released on keyboard on alive game, then toggle no Ko
handleEvent (EventKey (Char '\'') Up _ _) game@(Game _ _ _ _ _ _ _ _ Alive) = return $ toggleKo' game
-- ^ If s is released on keyboard on alive game, then sweep live zombies
handleEvent (EventKey (Char 's') Up _ (x, y)) game@(Game md _ _ _ _ s _ _ Alive) =
    return $ sweepLiveZombies game & bool id pipe (boardPoint s p)
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          pipe = if ehl |-> hlst /= Empty |-> isNothing sz then id
                 else p & bool reHighlightAt rePointHighlightAt (hlst == Ko)
          Meta _ _ _ _ (ehl,hlst,_) _ _ _ _ _ _ _ sz _ _ _ _ = md
-- ^ If z is released on keyboard on alive game, then toggle zombies
handleEvent (EventKey (Char 'z') Up _ (x, y)) game@(Game md _ _ _ _ s _ _ Alive) =
    return $ toggleZombies game & bool id pipe (boardPoint s p)
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          pipe = if ehl |-> hlst /= Empty |-> isJust zs && isNothing sz then id
                 else p & bool reHighlightAt rePointHighlightAt (hlst == Ko)
          Meta _ _ _ _ (ehl,hlst,_) _ _ _ _ _ zs _ sz _ _ _ _ = md
-- ^ If d is released on keyboard on alive game, then toggle decks of cards
-- ^ guard implies no pending letter
handleEvent (EventKey (Char 'd') Up _ _) game@(Game md _ _ _ _ _ _ _ Alive)
    | maybe False ((1==) . length) pl = return game
    | otherwise = return $ pendingText "dc" game
    where Meta _ _ _ pl _ _ _ _ _ _ _ _ _ _ _ _ _ = md

-- LEFT CLICK ---------------------------------------------------------- Dead --

-- ^ If clicked on point on dead game then take the point as part of hopeless string
handleEvent (EventKey (MouseButton LeftButton) Down _ (x, y)) game@(Game _ mm (eko,_) m lm s sb sw Dead)
    | seekBoard' m p /= Empty = return game'
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          sb' = if seekBoard game p == White then sb+1 else sb
          sw' = if seekBoard game p == Black then sw+1 else sw
          Game md _ _ _ _ _ _ _ _ = withUndoGame game game
          Meta is sp nr pl hl gs u r kos sko zs bz sz fu tr ps h = md
          Numbering e n ll tc = nr
          ll' = removePiece ll p
          nr' = Numbering e n ll' tc
          md' = Meta is sp nr' pl hl gs u r kos sko zs bz sz fu tr ps h
          m' = removePiece m p
          game' = Game md' mm (eko,[]) m' lm s sb' sw' Dead

-- MOTION ----------------------------------------------------- FOLLOWUP CARD --

-- ^ If motion on alive game at a followup Ko, then play the card
handleEvent event@(EventMotion (x, y)) game@(Game _ _ _ _ _ s _ _ status) =
    do
    kos <- fuPointsKo game
    case () of
      _ | status == Alive &&
          p `elem` kos -> playCard p game
      _                -> handleEvent' event game
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))

-- KEY ------------------------------------------------------------------ END --

-- ^ If e is pressed on keyboard on dead group then end the game
handleEvent (EventKey (Char 'e') Up _ _) game@(Game _ _ _ _ lm s _ _ Dead) = return $ finishGame game
-- ^ Ignore all other events
handleEvent _ game = return game

-- MOTION --------------------------------------------- STONE/POINT HIGHLIGHT --

-- | 'handleEvent'' handles mouse motion events on behalf of 'handleEvent'
handleEvent' :: Event -> Game -> IO Game

-- ^ If motion on alive game on the same group, then don't change highlighting if enabled and existing
-- ^ If motion on alive game at a stone not in the highlighted group, then re-highlighting
-- ^ guard implies no pending letter
handleEvent' (EventMotion (x, y)) game@(Game (Meta _ _ _ _ (True,hlst,(grp,_):_) _ _ _ _ _ _ _ _ _ _ _ _) _ _ _ _ _ _ _ _)
    | hl'grp && p `elem` grp = return game
    | hl'grp && st /= Empty = return $ bool (pipe game') game (zombiesPoint s p)
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          st = seekBoard' m p
          md' = Meta is sp nr Nothing (True,Empty,[]) gs u r kos' sko zs bz sz fu tr ps h
          game' = Game md' mm kos m lm s b w status
          pipe = reHighlightAt p
          hl'grp = boardPoint s p && getOppositeStone hlst /= Empty && maybe False ((`elem` ["hl", "gs"]) . take 2) pl
          Game md mm kos m lm s b w status = game
          Meta is sp nr pl (_,_,_) gs u r kos' sko zs bz sz fu tr ps h = md
-- ^ If motion on alive game on a stone, then start highlighting if enabled,
-- ^ If motion on alive game at a no stone point, then start point highlighting if enabled,
-- ^ unless there is a pending letter
handleEvent' (EventMotion (x, y)) game@(Game (Meta _ _ _ pl (True,_,_) _ _ _ _ _ _ _ _ _ _ _ _) _ _ m _ s _ _ _)
    | boardPoint s p && maybe False ((1==) . length) pl = return game
    | boardPoint s p = return $ pipe game
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))
          st = seekBoard' m p
          pipe = p & bool reHighlightAt rePointHighlightAt (st == Empty)

-- ^ If motion on alive game (along zombies), then cancel (point) highlighting if enabled
-- ^ unless zombies point belongs to some highlighted group
-- ^ LAST EQUATION FOR MOTION
handleEvent' (EventMotion (x, y)) game@(Game (Meta _ _ _ _ (True,hlst,_) _ _ _ _ _ _ _ _ _ _ _ _) _ _ _ _ s _ _ _)
    | hlst /= Empty = return $ cancelHighlight game p
    where p = Point (round ((x + 20*(fromIntegral s+1))/40)) (round ((-1*y + 20*(fromIntegral s+1))/40))

-- ^ Ignore all other motion events
handleEvent' _ game = pure game

-- Render ----------------------------------------------------------------------

-- | 'getStoneColor' returns the 'Stone' color
getStoneColor :: Stone -> Color
getStoneColor st = if st == Black then black else white

-- | 'render' functions gives picture of the Game that will be displayed on the window
render :: Game -> IO Picture
render game@(Game md mm _ m lm s sb sw status) = do
    kos <- fuPointsKo game
    let render_meta = [renderShowHelp (tc - length kos), renderStatus, renderPass, renderLabels kos, renderPointsKo kos, renderDecks decks]
    pictures_hl'gs <- renderHighlighting'GroupsSelected game
    let pictures_meta = bool (concatMap ($ game) render_meta) (renderDecks decks game) hide ++ pictures_hl'gs
    return $ pictures (pictures_Lines ++ pictures_handicap ++ pictures_zombies ++ bool [ (pictures stones), statusText] [] hide ++ pictures_score ++ pictures_meta ++ pictures_lastStone)
    where Meta _ _ _ pl _ _ _ _ _ _ _ _ _ (Numbering _ _ _ tc) _ _ _ = md
          hide = maybe False ((== "dc") . take 2) pl
          decks | status == Alive = getDecks (maybe False ((== "dc") . take 2) pl) game
                | otherwise = []
          -- Horizontal and vertical lines of the board
          gameHorizontalLines = [line [(fromIntegral (20 - 20*s),fromIntegral (20*s - 20 - 40*x)), (fromIntegral (40*s - 20 - 20*s),fromIntegral (20*s - 20 - 40*x))] | x <- [0..(s-1)]]
          gameVerticalLines = [line [(fromIntegral (20*s - 20 - 40*x),fromIntegral (20 - 20*s)), (fromIntegral (20*s - 20 - 40*x),fromIntegral (-20*s + 40*s - 20))] | x <- [0..(s-1)]]
          pictures_Lines = [ (pictures gameHorizontalLines), (pictures gameVerticalLines) ]
          -- Handicap points
          pictures_handicap = [ translate (fromIntegral (x*40 - 20*s-20))
                                          (fromIntegral (20*s - y*40 + 20))
                                        $ circleSolid 4
                              | Point x y <- points_handicap
                              ]
          points_handicap = [ Point x y
                            | let e = div s 2 + 1,
                              let a = div (div s 2) 2 + 1 - div s 19,
                              y <- [a,e,s+1-a], x <- [a,e,s+1-a],
                              s == 19 || not ((x == e || y == e) && x /= y)
                            ]
          -- All stones present on the game
          stones = [translate (fromIntegral (x*40 - 20*s-20)) (fromIntegral (20*s - y*40 + 20)) $ color c $ circleSolid 16 | (Point x y, st) <- (assocs m), st /= Ko, let c = getStoneColor st]
          -- Score of black stone
          scoreBlack = translate 0 (fromIntegral (20*s + 80)) $ scale 0.2 0.2 $ text $ "Black's Score : " ++ (show sb)
          -- Score of White stone
          scoreWhite = translate 0 (fromIntegral (20*s + 120)) $ scale 0.2 0.2 $ text $ "White's Score : " ++ (show sw)
          -- last stone
          pictures_lastStone | status == Alive = [ translate (fromIntegral (x*40 - 20*s-20))
                                                             (fromIntegral (20*s - y*40 + 20))
                                                           $ color c $ circle 14
                                                 | let lst = lastStone game,
                                                   not hide && isJust lst,
                                                   let (Point x y, st) = purify lst,
                                                   let c = getStoneColor (getOppositeStone st)
                                                 ]
                             | otherwise = []
          pictures_score = [ scoreBlack, scoreWhite ]
          -- Display turn if game is alive or instruction for capturing hopeless strings if game is dead oe winner if game is over
          turnsText = (show $ stone) ++ "'s turn"
          statusText = if status == Alive then translate (fromIntegral (-20*s))
                                                         (fromIntegral (-20*s - 120))
                                                       $ scale 0.4 0.4 $ text $ turnsText
                       else if status == Dead then translate (fromIntegral (-20*s))
                                                             (fromIntegral (-20*s - 120))
                                                           $ scale 0.2 0.2 $ text "Click on hopeless stones and enter e"
                       else translate (fromIntegral (-20*s))
                                      (fromIntegral (-20*s - 120))
                                    $ scale 0.4 0.4 $ text $ getWinner game
          stone = getTurnStoneFromMove lm mm
          pictures_zombies = let ps = map ($ game) [renderLiveZombies, renderDeadZombies]
                             in bool (concatMap fst ps ++ concatMap snd ps) [] hide

-- Render ---------------------------------------------------- Meta/Off Board --

-- | 'renderShowHelp' renders a short list of "usage" text for keys/clicks
renderShowHelp :: Int -> Game -> [Picture]
renderShowHelp _ (Game (Meta _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ False) _ _ _ _ _ _ _ _) = []
renderShowHelp tc game@(Game md maxMoves (eko,_) _ _ s _ _ _) =
          pictures_help
    where Meta _ _ _ _ (ehl,_,_) _ _ _ _ sko _ _ _ fu _ _ _ = md
          pictures_help = [ pass, undo, redo, urdo, hlng, mkos, noko, zmbs, trnr, idmm, nanr, lbtn ] ++
                          [ bool rbtn rbtn1 (maxMoves == 1) ]
          -- Show help
          pass = translate (fromIntegral (-20*s + 20))
                           (fromIntegral (20*s + 10+15*10))
                         $ scale 0.1 0.1 $ text $ "p pass"
          undo = translate (fromIntegral (-20*s + 20))
                           (fromIntegral (20*s + 10+15*9))
                         $ scale 0.1 0.1 $ color (bool red black $ isUndoSane game) $ text "u"
          redo = translate (fromIntegral (-20*s + 20))
                           (fromIntegral (20*s + 10+15*9))
                         $ scale 0.1 0.1 $ color (bool red black $ isRedoSane game) $ text "  r"
          urdo = translate (fromIntegral (-20*s + 20))
                           (fromIntegral (20*s + 10+15*9))
                         $ scale 0.1 0.1 $ text $ " /  undo/redo"
          hlng = translate (fromIntegral (-20*s + 20))
                           (fromIntegral (20*s + 10+15*8))
                         $ scale 0.1 0.1 $ color (bool black red ehl) $ text $ "g toggle highlighting"
          mkos = translate (fromIntegral (-20*s + 20))
                           (fromIntegral (20*s + 10+15*7))
                         $ scale 0.1 0.1 $ color (bool black red eko) $ text $ "\" taggle multiple Ko"
          noko = translate (fromIntegral (-20*s + 20))
                           (fromIntegral (20*s + 10+15*6))
                         $ color (bool (dim green) red sko) $ scale 0.1 0.1 $ text $ "' toggle Ko' (" ++ bool "speed" "sanity" sko ++ ")"
          zmbs = translate (fromIntegral (-20*s + 20))
                           (fromIntegral (20*s + 10+15*5))
                         $ scale 0.1 0.1 $ text $ "s/z sweep/zombies"
          trnr = translate (fromIntegral (-20*s + 20))
                           (fromIntegral (20*s + 10+15*4))
                         $ color (bool (bool black (dim blue) (enabled fu)) red (tc > 0)) $ scale 0.1 0.1 $ text $ "l/0 toggle/reset labels"
          idmm = translate (fromIntegral (-20*s + 20))
                           (fromIntegral (20*s + 10+15*3))
                         $ scale 0.1 0.1 $ text $ "-/+ decr/incr max moves (/2 *2)"
          nanr = translate (fromIntegral (-20*s + 20))
                           (fromIntegral (20*s + 10+15*2))
                         $ scale 0.1 0.1 $ text $ "1/a numeric/alphabetic numbering"
          lbtn = translate (fromIntegral (-20*s + 20))
                           (fromIntegral (20*s + 10+15*1))
                         $ scale 0.1 0.1 $ text $ "left btn - place stone | capture group"
          rbtn = translate (fromIntegral (-20*s + 20))
                           (fromIntegral (20*s + 10))
                         $ scale 0.1 0.1 $ text $ "middle/right btn - skip turn | multi select|capture/undo"
          rbtn1 = translate (fromIntegral (-20*s + 20))
                            (fromIntegral (20*s + 10))
                          $ scale 0.1 0.1 $ text $ "middle/right btn - pending letter | multi undo"

-- | 'renderStatus' renders the status: remaining moves, pending letter, count of groups/stones/liberties in the highlight
renderStatus :: Game -> [Picture]
renderStatus game@(Game md maxMoves _ _ _ s _ _ status) =
    statusText'
    where Meta _ _ _ pl (ehl,hlst,m'hl) _ _ _ _ _ _ _ _ _ _ _ _ = md
          movesText = if maxMoves == 1 then ""
                      else (if moves < maxMoves then " [" else " (") ++
                           show moves ++ "/" ++ show maxMoves ++
                           (if moves < maxMoves then "]" else ")")
          statusText' = statusText1 ++ statusText2 ++ statusText3
          statusText1 = if status /= Alive then []
                        else [ translate (fromIntegral (120 - (s-9)*20))
                                         (fromIntegral (-20*s - 115))
                                       $ scale 0.2 0.2 $ text $ movesText ]
          statusText2 = case pl of
                          Just l@[_] ->
                            [ translate (fromIntegral (120 - (s-9)*20))
                                        (fromIntegral (-20*s - 115))
                                      $ scale 0.2 0.2 $ text $ " '" ++ l ++ "..." ]
                          _          -> []
          statusText3 = case pl of
                          Just t | length t >= 3 && t !! 2 /= ' ' ->
                            let t' = maybe t (`take` t) (elemIndex ' ' t) in
                            [ translate (fromIntegral (120 - (s-9)*20))
                                        (fromIntegral (-20*s - 155))
                                      $ scale 0.15 0.15 $ text $ " > " ++ t' ++ " <" ]
                          Just t | length t == 2 ->
                            if ehl && hlst /= Empty && ((`elem` ["hl", "gs"]) . take 2 $ t)
                            then
                                let stat'hl st grp lbs d = [ translate (fromIntegral (120 - (s-9)*20))
                                                                       (fromIntegral (-20*s - 155 - d))
                                                                     $ scale 0.1 0.1 $ st'text
                                                           , translate (fromIntegral (175 - (s-9)*20))
                                                                       (fromIntegral (-20*s - 155 - d))
                                                                     $ scale 0.1 0.1 $ st''text
                                                           , translate (fromIntegral (115 - (s-9)*20))
                                                                       (fromIntegral (-20*s - 150 - d))
                                                                     $ st'color $ circleSolid 8
                                                           ] ++
                                                           [ translate (fromIntegral (170 - (s-9)*20))
                                                                       (fromIntegral (-20*s - 150 - d))
                                                                     $ color black $ axis | axis <- st'cross
                                                           ] ++
                                                           [ translate (fromIntegral (170 - (s-9)*20))
                                                                       (fromIntegral (-20*s - 150 - d))
                                                                     $ st'color $ circleSolid 2
                                                           ]
                                        where st'text = text $ " " ++ show (length grp)
                                              st''text = text $ " " ++ show (length lbs)
                                              st'color = color (getStoneColor st)
                                              st'delta = [ (dx,dy) | dx <- [0,1], dy <- [0,1], dx + dy == 1 ]
                                              st'cross = [ line [(fromIntegral (-dx*10), fromIntegral (-dy*10))
                                                                ,(fromIntegral (dx*10), fromIntegral (dy*10))]
                                                         | (dx, dy) <- st'delta
                                                         ]
                                in if hlst == Ko
                                   then let black'hl' = getMultiSele Black game
                                     in let (black'hl'grp, black'hl'lbs) = uniMultiSele Black game
                                     in let white'hl' = getMultiSele White game
                                     in let (white'hl'grp, white'hl'lbs) = uniMultiSele White game
                                     in let stat'hl' n' d = [ translate (fromIntegral (80 - (s-9)*20))
                                                                        (fromIntegral (-20*s - 155 - d))
                                                                      $ scale 0.1 0.1 $ text $ " " ++ show n'
                                                            ]
                                     in if length black'hl'lbs + length white'hl'lbs == 0 then []
                                        else stat'hl Black black'hl'grp black'hl'lbs 0 ++ stat'hl' (length black'hl') 0
                                          ++ stat'hl White white'hl'grp white'hl'lbs 25 ++ stat'hl' (length white'hl') 25
                                   else let (grp,lbs):_ = m'hl
                                        in stat'hl hlst grp lbs (bool 25 0 (hlst == Black))
                            else []
                          _                                       -> []
          moves = getMaxMovesFromLastMove game

-- | 'renderPass' dislays a stone under the score if the last move is a Pass, or a circle for a Skip
renderPass :: Game -> [Picture]
renderPass (Game _ _ _ _ lm s _ _ status) =
    pictures_pass
    where passHint g c = translate 220 (fromIntegral (20*s + 50)) $ color c $ g 16
          passFlag | status == Alive = [ passHint (snd p) (getStoneColor st)
                                       | st <- [Black,White],
                                               let p = case lm of
                                                         Pass{} -> (getPiece, circleSolid)
                                                         Move{} -> (const Ko, circle)
                                                         _      -> (getPiece, circle),
                                               fst p lm == st
                                       ]
                   | otherwise = []
          pictures_pass = passFlag

-- Render ------------------------------------------------------------ Labels --

-- | 'renderLabels' displays labels on stones using opposite color, and/or labels on followup points
renderLabels :: [BoardGo.Point] -> Game -> [Picture]
renderLabels kos'' game@(Game md _ _ _ _ _ _ _ _)
    | hide = []
    | otherwise = pictures_stoneLabels ++ pictures_pointLabels
    where Meta _ _ nr pl _ _ _ _ _ _ _ _ _ _ _ _ _ = md
          hide = maybe False ((== "dc") . take 2) pl
          pictures_stoneLabels | enabled nr = renderStoneLabels game (getLabels True nr, getLabels False nr)
                               | otherwise = []
          pictures_pointLabels = renderPointLabels game kos''

-- | 'renderPointLabels' displays labels on followup points
renderPointLabels :: Game -> [BoardGo.Point] -> [Picture]
renderPointLabels (Game md mm _ _ lm s _ _ _) kos'' =
    pointNumbers' ++ pointNumbers ++ pointLetters'' ++ pointLetters
    where Meta _ _ _ _ _ _ _ _ _ _ _ _ _ fu _ _ _ = md
          -- point labels
          pointNumbers' = [ translate (fromIntegral (x*40 - 20*s-20))
                                      (fromIntegral (20*s - y*40 + 20))
                                    $ color (dark yellow) $ rectangleSolid (nr'w n) (nr'h n)
                          | (n, Point x y, _, _, _) <- ptnr
                          ]
          pointNumbers = [ translate (fromIntegral (x*40 - 20*s - 30 + dx))
                                     (fromIntegral (20*s - y*40 + 10 + dy))
                                   $ color pt'c $ scale sc sc $ text $ show n
                         | (n, Point x y, dx, dy, sc) <- ptnr
                         ]
          ptnr = [ (n, p, dx, dy, sc)
                 | (p, n) <- ptnr',
                             let (dx, dy, sc) = (nr'dx n, nr'dy n, nr'sc n)
                 ]
          ptnr' | enabled fu = getLabels True fu
                | null kos'' = []
                | otherwise = getLabels True fu{enabled=True}
                            & sortOn snd
                            & take (length kos'')
          pointLetters'' = [ translate (fromIntegral (x*40 - 20*s-20))
                                       (fromIntegral (20*s - y*40 + 20))
                                     $ color (dark yellow) $ rectangleSolid 20 20
                           | (_, Point x y) <- ptlr
                           ]
          pointLetters = [ translate (fromIntegral (x*40 - 20*s - 30 + 3))
                                     (fromIntegral (20*s - y*40 + 10 + 3))
                                   $ color pt'c $ scale 0.14 0.14 $ text $ take 1 (drop 1 (show l))
                         | (l, Point x y) <- ptlr
                         ]
          ptlr = [ (chr n, p) | (p, n) <- ptlr' ]
          ptlr' | enabled fu = getLabels False fu
                | null kos'' = []
                | otherwise = getLabels False fu{enabled=True}
                            & sortOn snd
                            & take (length kos'')
          pt'c = getStoneColor (getTurnStoneFromMove lm mm)
          --
          nr'dx n | n < 10 = 3 | n < 100 = -1 | otherwise = -2
          nr'dy n | n < 10 = 3 | n < 100 = 3 | otherwise = 5
          nr'sc n | n < 10 = 0.14 | n < 100 = 0.12 | otherwise = 0.09
          nr'w n | n < 10 = 20 | otherwise = 30
          nr'h n | n < 10 = 20 | otherwise = 20

-- | 'renderStoneLabels' displays labels on (possibly decks board's) stones using opposite color
renderStoneLabels :: Game -> ([(BoardGo.Point, Int)], [(BoardGo.Point, Int)]) -> [Picture]
renderStoneLabels (Game md _ _ m _ s _ _ _) (l'nr, l'lr) =
    pictures_stoneLabels
    where Meta _ _ _ _ _ _ _ _ _ _ zs _ sz _ _ _ _ = md
          -- stone
          pictures_stoneLabels = stoneNumbers ++ stoneLetters ++ gridNumbers ++ gridLetters
          stoneNumbers = [ translate (fromIntegral (x*40 - 20*s - 30 + dx))
                                     (fromIntegral (20*s - y*40 + 10 + dy))
                                   $ color c $ scale sc sc $ text $ show n
                         | (n, Point x y, dx, dy, c, sc) <- stnr
                         ]
          stnr = [ (n, p, dx, dy, c, sc)
                 | (p, n) <- l'nr,
                             let st = seekBoard' m p,
                             let c = getStoneColor' st,
                             let (dx, dy, sc) = (nr'dx n, nr'dy n, nr'sc n)
                 ]
          stoneLetters = [ translate (fromIntegral (x*40 - 20*s - 30 + 3))
                                     (fromIntegral (20*s - y*40 + 10 + 3))
                                   $ color c $ scale 0.14 0.14 $ text $ take 1 (drop 1 (show l))
                         | (l, Point x y, c) <- stlr
                         ]
          stlr = [ (chr n, p, c)
                 | (p, n) <- l'lr,
                             let st = seekBoard' m p,
                             let c = getStoneColor' st
                 ]
          -- grid
          gridNumbers = [ translate (fromIntegral (x*40 - 20*s - 30 + dx))
                                    (fromIntegral (-10 + 20*s - y*40 + 10 + dy))
                                  $ color (dim blue) $ scale 0.15 0.15 $ text $ show n
                        | (n, Point x y, dx, dy) <- grnr
                        ]
          gridLetters = [ translate (fromIntegral (-10 + (s+1-x)*40 - 20*s-20))
                                    (fromIntegral (-10 + 20*s - y*40 + 20))
                                  $ color (dim blue) $ scale 0.15 0.15 $ text $ rnrs !! n
                        | (n, Point y x, _, _) <- grnr
                        ]
          grnr = [ (n, Point n (bool (-1) 0 (isNothing zs && isNothing sz)), dx, dy)
                 | n <- [1..s],
                        let (dx, dy) = (nr'dx n, nr'dy n)
                 ]
          rnrs = [ "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX"]
          nr'dx n | n < 10 = 3 | n < 100 = -1 | otherwise = -4
          nr'dy n | n < 10 = 3 | n < 100 = 3 | otherwise = 5
          nr'sc n | n < 100 = 0.14 | n < 1000 = 0.11 | otherwise = 0.09
          getStoneColor' = getStoneColor . getOppositeStone

-- Render ---------------------------------------------------------------- Ko --

-- | 'renderPointsKo' displays a square for the Ko point(s), in the opposite color of the forbidden stone,
-- ^ or a red square for the Ko' (prime) points - sanity enabled and playing a move already played
renderPointsKo :: [BoardGo.Point] -> Game -> [Picture]
renderPointsKo kos'' game@(Game md mm _ _ lm s _ _ _) =
    pictures_squaresKo
    where Meta _ _ _ _ _ _ _ _ kos' sko _ _ _ _ _ _ _ = md
          -- centered Ko square
          squareKo c = \x y -> translate (fromIntegral (x*40 - 20*s - 20))
                                         (fromIntegral (20*s - y*40 + 20))
                                       $ color c $ rectangleWire 20 20
          squaresKo = [ squareKo (c'Ko p) x y | p@(Point x y) <- kos ]
          squaresKo' = [ squareKo red x y | Point x y <- kos' ]
          squaresKo'' = [ squareKo (dim blue) x y | Point x y <- kos'' ]
          pictures_squaresKo = squaresKo ++ squaresKo' ++ squaresKo''
          kos = bool id (\\ kos') sko . getPointsKo $ game
          c'Ko p = getStoneColor . bool getOppositeStone id (validKo game Nothing p) $ stone
          stone = getTurnStoneFromMove lm mm

-- | 'renderDecks' draws a blue underscore below a stone with cards (a deck)
renderDecks ::  [((BoardGo.Point, Stone), (Maybe Int, Game))] -> Game -> [Picture]
renderDecks decks game@(Game md _ _ _ _ s _ _ _) =
    pictures_Decks ++ bool stones [] hide ++ bool pictures_Labels [] hide
    where Meta _ _ nr pl _ _ _ _ _ _ _ _ _ _ _ _ _ = md
          hide = maybe False ((/= "dc") . take 2) pl
          points = map (fst . fst) decks
          labels = map (fst . snd) decks
          stlr = zip points labels
               & List.filter (isJust . snd)
               & map (second purify)
          l'nr = List.filter ((< 0) . snd) stlr
               & map (second (0-))
          l'lr = List.filter ((> 0) . snd) stlr
          -- All deck stones
          stones = [ translate (fromIntegral (x*40 - 20*s-20))
                               (fromIntegral (20*s - y*40 + 20))
                             $ color c $ circleSolid 16
                   | ((Point x y, st), _) <- decks,
                                             let c = getStoneColor st
                   ]
          -- All deck labels
          pictures_Labels | enabled nr = renderStoneLabels game (l'nr, l'lr)
                          | otherwise = []
          pictures_Decks = map draw points
          ((x1, y1), (x2, y2)) = ((-20+4, -20-2), (20-4, -20-2))
          path = [(fromIntegral x1,fromIntegral y1)
                 ,(fromIntegral x2,fromIntegral y2)]
          draw (Point x y) = translate (fromIntegral (x*40 - 20*s-20))
                                       (fromIntegral (20*s - y*40 + 20))
                                     $ color (dim blue) $ line path

-- Render ----------------------------------------------------------- Zombies --

-- | 'renderLinesZombies' renders the dashed (almost dotted) lines to the live/dead zombies
renderLinesZombies :: Game -> [BoardGo.Point] -> Int -> [Picture]
renderLinesZombies (Game _ _ _ _ _ s _ _ _) zs dash =
    pictures_Lines_zombies
    where -- Horizontal zombies lines of the board
          horz_dashed_zombies :: Int -> (Int, Int) -> (Int, Int) -> [Path]
          horz_dashed_zombies dx (x1, y1) (_, y2) = [ [(fromIntegral (x1+2*n*dx),fromIntegral y1)
                                                      ,(fromIntegral (x1+2*n*dx+dx),fromIntegral y2)]
                                                    | n <- [0 .. div 20 (abs dx) - 1] -- 2 x 10 | 4 x 5
                                                    ]
          gameHorizontalLines_zombies_left = concat [ map line (horz_dashed_zombies dash (-20 - 20*s, 20*s - 20 - 40*y)
                                                                                         (20 - 20*s, 20*s - 20 - 40*y))
                                                    | y <- [0..s-1],
                                                            Point 0 (y+1) `elem` zs
                                                    ]
          gameHorizontalLines_zombies_right = concat [ map line (horz_dashed_zombies (-dash) (20*s + 20, 20*s - 20 - 40*y)
                                                                                             (20*s - 20, 20*s - 20 - 40*y))
                                                     | y <- [0..s-1],
                                                            Point (s+1) (y+1) `elem` zs
                                                     ]
          gameHorizontalLines_zombies = gameHorizontalLines_zombies_left ++ gameHorizontalLines_zombies_right
          -- Vertical zombies lines of the board
          vert_dashed_zombies :: Int -> (Int, Int) -> (Int, Int) -> [Path]
          vert_dashed_zombies dy (x1, y1) (x2, _) = [ [(fromIntegral x1,fromIntegral (y1+2*n*dy))
                                                      ,(fromIntegral x2,fromIntegral (y1+2*n*dy+dy))]
                                                    | n <- [0 .. div 20 (abs dy) - 1] -- 2 x 10 | 4 x 5
                                                    ]
          gameVerticalLines_zombies_up = concat [ map line (vert_dashed_zombies (-dash) (20*s - 20 - 40*x, 20*s + 20)
                                                                                        (20*s - 20 - 40*x, 20*s - 20))
                                                | x <- [0..s-1],
                                                       Point (s-x) 0 `elem` zs
                                                ]
          gameVerticalLines_zombies_down = concat [ map line (vert_dashed_zombies dash (20*s - 20 - 40*x, -20 - 20*s)
                                                                                       (20*s - 20 - 40*x, 20 - 20*s))
                                                  | x <- [0..s-1],
                                                         Point (s-x) (s+1) `elem` zs
                                                  ]
          gameVerticalLines_zombies = gameVerticalLines_zombies_up ++ gameVerticalLines_zombies_down
          -- Horizontal and vertical zombies lines of the board
          pictures_Lines_zombies = gameHorizontalLines_zombies ++ gameVerticalLines_zombies

-- | 'renderDeadZombies' displays stones without a middle ring for the dead zombies
renderDeadZombies :: Game -> ([Picture], [Picture])
renderDeadZombies game@(Game (Meta _ _ _ _ _ _ _ _ _ _ zs bz _ _ _ _ _) _ _ _ _ s _ _ _) =
    (pictures_LinesDeadZombies, pictures_DeadZombies)
    where -- All dead zombies stones present on the game
          pictures_LinesDeadZombies = renderLinesZombies game (keys zs'sz) 2 ++
                                      renderLinesZombies game (keys zs') 4
          pictures_DeadZombies = zombies ++ zombies' ++ zombies'' ++ zombies'''
          zombies = [ translate (fromIntegral (x*40 - 20*s-20))
                                (fromIntegral (20*s - y*40 + 20))
                              $ color c $ circleSolid 16
                    | (Point x y, st) <- assocs zs'sz,
                                         let c = getStoneColor st
                    ]
          zombies' = [ translate (fromIntegral (x*40 - 20*s-20))
                                 (fromIntegral (20*s - y*40 + 20))
                               $ color (dark yellow) $ circleSolid 10
                     | (Point x y, _) <- assocs zs'sz
                     ]
          zombies'' = [ translate (fromIntegral (x*40 - 20*s-20))
                                  (fromIntegral (20*s - y*40 + 20))
                                $ color c $ circleSolid 8
                      | (Point x y, st) <- assocs zs'sz,
                                           let c = getStoneColor st
                      ]
          zombies''' = [ translate (fromIntegral (x*40 - 20*s-20))
                                   (fromIntegral (20*s - y*40 + 20))
                                 $ color (dark yellow) $ circleSolid 1
                       | (Point x y, _) <- assocs bz
                       ]
          zs' = fromMaybe Map.empty zs
          zs'sz = bz
                  `Map.union`
                  getDeadZombies game

-- | 'renderLiveZombies' displays stones with a center disc-hole for the live zombies
renderLiveZombies :: Game -> ([Picture], [Picture])
renderLiveZombies game@(Game (Meta _ _ _ _ _ _ _ _ _ _ zs _ _ _ _ _ _) _ _ _ _ s _ _ _) =
    (pictures_LinesLiveZombies, pictures_LiveZombies)
    where -- All dead zombies stones present on the game
          pictures_LinesLiveZombies = renderLinesZombies game sz'zs' 2
          pictures_LiveZombies = zombies ++ zombies'
          zombies = [ translate (fromIntegral (x*40 - 20*s-20))
                                (fromIntegral (20*s - y*40 + 20))
                              $ color c $ circleSolid 16
                    | (Point x y, st) <- assocs sz'zs,
                                         let c = getStoneColor st
                    ]
          zombies' = [ translate (fromIntegral (x*40 - 20*s-20))
                                 (fromIntegral (20*s - y*40 + 20))
                               $ color (dark yellow) $ circleSolid 4
                     | (Point x y, _) <- assocs sz'zs
                     ]
          sz'zs = getLiveZombies game
          sz'zs' = keys sz'zs \\ maybe [] keys zs

-- Render -------------------------------------------------------------- Wire --

-- | 'renderWire' draws lines of exactly the size of a square
-- ^ that "enclose" (highlight) a group of stones by a "wire"
renderWire :: [Wire] -> Int -> Color -> [Picture]
renderWire ws s c = pictures_Wire
    where pictures_Wire = map draw ws
          draw (Point x y, (dx,dy)) = translate (fromIntegral (x*40 - 20*s-20))
                                                (fromIntegral (20*s - y*40 + 20))
                                    . color c
                                    $ line path
              where p | dx /= 0 = ((20*dx, -20), (20*dx, 20))
                      | dy /= 0 = ((-20, -20*dy), (20, -20*dy))
                    ((x1, y1), (x2, y2)) = p
                    path = [(fromIntegral x1,fromIntegral y1)
                           ,(fromIntegral x2,fromIntegral y2)]

-- | 'renderWire'' draws a wire that fits within a wire rendered by 'renderWire'
renderWire' :: Game
            -> Int
            -> Color -> [Picture]
renderWire' (Game md _ _ _ _ s _ _ _) d c = pictures_Wire
    where Meta _ _ _ _ _ (gw,_) _ _ _ _ _ _ _ _ _ _ _ = md
          pictures_Wire = map (draw . second (bimap (*d) (*d))) gw
          draw ((Point x y, (dx,dy)), (mind, maxd)) = translate (fromIntegral (x*40 - 20*s-20))
                                                                (fromIntegral (20*s - y*40 + 20))
                                                    . color c
                                                    $ line path
              where p | dx /= 0 = (((20+d)*dx, (20+d)-2*mind), ((20+d)*dx, -(20+d)+2*maxd))
                      | dy /= 0 = ((-(20+d)+2*mind, -(20+d)*dy), ((20+d)-2*maxd, -(20+d)*dy))
                    ((x1, y1), (x2, y2)) = p
                    path = [(fromIntegral x1,fromIntegral y1)
                           ,(fromIntegral x2,fromIntegral y2)]

-- | 'renderHighlighting' displays straight lines (wire) around stones highlighting the group
-- ^ and green dots (unless red from groups selected) at the group's liberty points
renderHighlighting :: Game -> [BoardGo.Point] -> ([BoardGo.Point], [Picture])
renderHighlighting game@(Game md _ _ _ _ _ _ _ _) lbs' =
    renderHighlight game hlst ws lbs lbs'
    where Meta _ _ _ _ (True,hlst,_) _ _ _ _ _ _ _ _ _ _ _ _ = md
          ws = getHighlight hlst game
          lbs = snd $ uniMultiSele hlst game

-- | 'renderHighlight' is used from 'renderHighlighting' or 'renderPointHighlighting'
-- ^ and uses 'renderWire' to draw an enclosing "wire" around and along the stones
renderHighlight :: Game
                -> Stone
                -> [Wire]
                -> [BoardGo.Point]
                -> [BoardGo.Point] -> ([BoardGo.Point], [Picture])
renderHighlight (Game _ mm _ _ lm s _ _ _) hlst ws lbs lbs' =
    (if hlst /= stone && length lbs == 1 then [] else lbs, pictures_Wire)
    where pictures_Wire = renderWire ws s c
          c | hlst == stone = bright blue
            | null $ intersect lbs lbs' = bright blue
            | otherwise = red
          stone = getTurnStoneFromMove lm mm

-- | 'renderPointHighlighting' displays straight lines (wire) around stones, highlighting the groups
-- ^ sharing the same liberty, and green dots (unless red from groups selected) for each group's liberty points
renderPointHighlighting :: Game -> [BoardGo.Point] -> ([BoardGo.Point], [Picture])
renderPointHighlighting (Game (Meta _ _ _ _ (_,Empty,_) _ _ _ _ _ _ _ _ _ _ _ _) _ _ _ _ _ _ _ _) _ = ([], [])
renderPointHighlighting game@(Game (Meta _ _ _ _ (True,Ko,_) _ _ _ _ _ _ _ _ _ _ _ _) _ _ _ _ _ _ _ _) lbs' =
    let (black'lbs, black'ps) = rh Black black'ws (snd black'hl) in
    let (white'lbs, white'ps) = rh White white'ws (snd white'hl) in
    (black'lbs ++ white'lbs & nub, black'ps ++ white'ps)
    where (black'ws, black'hl) = (getHighlight Black game, uniMultiSele Black game)
          (white'ws, white'hl) = (getHighlight White game, uniMultiSele White game)
          rh st ws lbs = renderHighlight game st ws lbs lbs'
renderPointHighlighting _ _ = ([], [])

-- | 'renderGroupsSelected' displays wires around each group selected, and returns also
-- ^ the (drawn in red) liberty points that are shared between all groups, and so that
-- ^ can be played in a multi-move
renderGroupsSelected :: Game -> IO ([BoardGo.Point], [Picture])
renderGroupsSelected game@(Game md mm _ _ lm _ _ _ _)
    | not hide = do
                 lbs <- libertiesFitOfGS game
                 return (lbs, pictures_Wire)
    where Meta _ _ _ pl _ _ _ _ _ _ _ _ _ _ _ _ _ = md
          hide = maybe False ((== "dc") . take 2) pl
          pictures_Wire = renderWire' game 2 (getStoneColor stone)
          stone = getTurnStoneFromMove lm mm
renderGroupsSelected _ = return ([], [])

-- | 'renderHighlighting'GroupsSelected' uses the (red) liberty points from 'renderGroupsSelected' to
-- ^ let either 'renderPointHighlighting' or 'renderHighlighting' not to render the (green) liberty points
renderHighlighting'GroupsSelected :: Game -> IO [Picture]
renderHighlighting'GroupsSelected game@(Game (Meta _ _ _ _ (False,_,_) _ _ _ _ _ _ _ _ _ _ _ _) _ _ _ _ _ _ _ _) =
    do
    (_, rgs) <- renderGroupsSelected game
    return rgs
renderHighlighting'GroupsSelected game@(Game md _ _ _ _ s _ _ _)
    | not hide = do
                 (lbs', rgs) <- renderGroupsSelected game
                 let pictures_Free' = map (draw red) lbs'
                 let (lbs, rhl) = bool renderHighlighting renderPointHighlighting (hlst == Ko) game lbs'
                 let pictures_Free = map (draw $ dim green) (lbs \\ lbs')
                 return $ rgs ++ pictures_Free' ++ rhl ++ pictures_Free
    where Meta _ _ _ pl (_,hlst,_) _ _ _ _ _ _ _ _ _ _ _ _ = md
          hide = maybe False ((== "dc") . take 2) pl
          draw c (Point x y) = translate (fromIntegral (x*40 - 20*s-20))
                                         (fromIntegral (20*s - y*40 + 20))
                                       $ color c $ circleSolid 4
renderHighlighting'GroupsSelected _ = return []
