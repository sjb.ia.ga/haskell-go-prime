{-|
    This module contains 'main' fuction.
-}
module Main(main) where

import Go

-- | The 'main' functions calls 'playGo'.
main :: IO()
main = playGo
