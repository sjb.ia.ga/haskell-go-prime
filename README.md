# haskell-go-prime

This is a "_toy_" project.

WARNING! The CPUs usage rate can become very high and overheat the board!


## Go in Haskell

This project is a fork of [haskell-go-checkers](https://github.com/prateekkumarweb/haskell-go-checkers).

## Wiki

For more details, visit the [wiki](https://gitlab.com/sjb.ia.ga/haskell-go-prime/-/wikis/Haskell-Go-Prime) page.

## Description

The game is written in Haskell: the programming style is "vertical" - a "`pipe`"
of functions are applied either through the application (`$` or `&`) or
the composition (`.`) operators, with heavy use of `uncurry`. As it was
coded without the benefit of a debugger, the gain is that the "comment-line re-run"
cycles allow simple debugging and/or fast testing.

## Visuals

## Usage

## Authors and acknowledgment
Thanks to the original authors: Prateek Kumar, Vaibhav Sinha.

## License
MIT

## Project status

Beta

There is also a [sacred](https://gitlab.com/sjb.ia.ga/haskell-go-prime/-/tree/sacred) branch.
